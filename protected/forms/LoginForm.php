<?php

/**
 * Class LoginForm
 * Класс формы логина
 */

class LoginForm extends CFormModel{

    public $username;
    public $password;
    public $remember_me;

    private $identity;

    public function rules(){
        return array(
            array('username', 'required', 'message' => 'Email не может быть пустым'),
            array('password', 'required', 'message' => 'Пароль не может быть пустым'),
            array('username', 'email', 'message' => 'Неверный формат Email'),
            array('remember_me', 'boolean'),
        );
    }

    public function attributeLabels(){
        return array(
            'username' => 'Электронная почта',
            'password' => 'Пароль',
            'remember_me' => 'Запомнить меня'
        );
    }

    public function login()
    {
        if($this->identity===null){
            $this->identity=new UserIdentity($this->username,$this->password);
            $this->identity->authenticate();
        }

        if($this->identity->errorCode===UserIdentity::ERROR_NONE){
            $duration = $this->remember_me ? 3600*24*30 : 0; // 30 days
            Yii::app()->user->login($this->identity, $duration);
            return true;
        }
        else
            return false;
    }
}