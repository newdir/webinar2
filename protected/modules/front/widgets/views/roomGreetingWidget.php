<div class="greeting-speaker">

    <?php if ($conference['type_id'] !== 2): ?>

        <div class="row conference-name">
            <p><?= $room['number_in_folder']; ?>-й день конференции "<?= $conference['name']; ?>"</p>
        </div>
        <div class="row conference-date">
            <p><?= $room['date'] ?> с <?= substr($speakers[0]['date_perfomance'], 11, 5) ?> (мск)</p>
        </div>

    <?php endif; ?>

    <?php if (!empty($speakers)): ?>
        <?php foreach ($speakers as $speaker): ?>
            <div class="row row-speaker">
                <div class="col-xs-2 conference-speaker">
                    <div class="row speaker-photo">
                        <div class="speaker-front-photo" style="background-image: url(<?= $speaker['img'] ?>)"></div>
                    </div>
                </div>
                <div class="col-xs-10 conference-info">
                    <div class="row">
                        <div class="col-xs-12 conference-info-date">
                            <div class="conference-time">
                                <p><?= $room['date'] ?> с <?= substr($speaker['date_perfomance'], 11, 5) ?></p>
                            </div>
                            <div class="conference-region">
                                <p>Время московское</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 speaker-theme">
                            <p><?= $speaker['theme_perfomance'] ?></p>
                        </div>
                    </div>
                    <div class="row conference-features">

                        <?php if (!empty($speaker['description'])): ?>
                            <div class="col-xs-12 features-label">
                                <p>Из мастер - класса Вы узнаете:</p>
                            </div>
                            <div class="col-xs-10 features">
                                <?= BulletHtml::get($speaker['description']); ?>
                            </div>

                            <?php if (!empty($speaker['video'])): ?>

                                <div class="col-xs-10 features">

                                    <?= preg_replace(["/width=\"\d+\"/", "/height=\"\d+\"/"], ['width="480"', 'height="360"'], $speaker['video']) ?>

                                </div>

                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if (!empty($speaker['materials']) || !empty($speaker['files'])): ?>
                            <div class="col-xs-12 conference-materials">
                                <div class="col-xs-12 materials-label">
                                    <p>Материалы и инструменты для мастер - класса:
                                        <?php
                                        if (!empty($speaker['files'])) {
                                            $files = explode(',', $speaker['files']);

                                            if (count($files) === 1) {
                                                echo '<a target="_blank" href="' . $files[0] . '">Скачать материалы</a>';
                                            } else {
                                                $index = 1;

                                                foreach ($files as &$file) {
                                                    $file = '<a target="_blank" href="' . $file . '">Файл' . $index++ . '</a>';
                                                }

                                                echo implode(', ', $files);
                                            }
                                        }
                                        ?>
                                    </p>
                                </div>
                                <?php if (!empty($speaker['materials'])) : ?>
                                    <div class="col-xs-10 features">
                                        <?= BulletHtml::get($speaker['materials']) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>

            <div class="row spaker-images">
                <div class="col-xs-offset-2 col-xs-10">

                    <?php if (!empty($speaker['images'])): ?>

                        <?php foreach (explode(',', $speaker['images']) as $image):?>
                            <img class="speaker-image" src="<?= $image ?>">
                        <?php endforeach; ?>

                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>