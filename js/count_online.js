setInterval(function (){

    $.ajax({
        url: '/front/default/countOnline/?room_id=' + $('.main-content').data('room_id'),
        method: 'GET',
        dataType: 'json',
        success: function (data) {
            if (data.success === true) {
                $('.header-audience').text('Зрителей онлайн: ' + data.count);
            }
        }
    });
}, 60000);