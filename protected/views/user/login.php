<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WHD6Z8V');</script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-theme.min.css">

    <link href="/css/login.css" rel="stylesheet">
    <link href="/css/common-login.css" rel="stylesheet">

    <title><?= Yii::app()->name . ' - Login'; ?></title>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHD6Z8V"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="container">
    <div class="login-wrap">
        <div class="row">
            <div class="col-xs-11 form-title">
                <h2>Новые Направления</h2>
                <p>Организация международных конференций</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 form-wrap">
                <?php $form = $this->beginWidget('CActiveForm', array('id' => 'login-form', 'htmlOptions' => array('class' => "form-horizontal", 'role' => "form"))); ?>
                    <div class="form-group">
                        <?= $form->label($model, 'username', array('class' => 'col-xs-3 control-label')); ?>
                        <div class="col-xs-6">
                            <?= $form->textField($model, 'username', array("type" => "email", "class" => "form-control", "placeholder" => "Электронная почта")); ?>
                            <?= $form->error($model, 'username'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $form->label($model, 'password', array('class' => 'col-xs-3 control-label')); ?>
                        <div class="col-xs-6">
                            <?= $form->passwordField($model, 'password', array("class" => "form-control", "placeholder" => "Пароль")); ?>
                            <?= $form->error($model, 'password'); ?>
                            <?php if(!empty($errors)): ?>
                                <p style="color: #CC0000"><?= $errors[0] ?></p>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-xs-offset-3 remember">
                            <?= $form->checkBox($model, 'remember_me');?>
                            <?= $form->label($model, 'remember_me');?>
                        </div>
                    </div>
                    <button type="submit" class="col-xs-offset-4 btn-submit">Войти</button>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>