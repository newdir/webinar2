<!DOCTYPE html>
<html lang="en">
<head>
    <title>Вебинар.<?= FullConferenceInfo::getTitle(Yii::app()->request->getParam('conf_id')) ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/css-fonts.css?v=1.0">

    <?php $conference_info = CJSON::decode($this->widget('application.modules.front.widgets.RoomJsonWidget', [], true), true); ?>

    <script type="text/javascript">
        var room_json = <?php $this->widget('application.modules.front.widgets.RoomJsonWidget'); ?>;
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript" src="/js/vendors/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="text/javascript" src="/js/respond.js"></script>

    <script type="text/javascript" src="/js/vendors/jquery/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/jquery.fancybox.css">
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
    <script type="text/javascript" src="/js/refresh.js"></script>

    <script src="https://cdn.socket.io/socket.io-1.3.5.js"></script>
    <script type="text/javascript" src="/js/vendors/jquery/jQuery.cookie.js"></script>

    <script type="text/javascript" src="/js/likely.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/correction-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/front.css?v=2.3">

    <link rel="stylesheet" type="text/css" href="/css/front_upd.css?v=1.4">
    <link rel="stylesheet" type="text/css" href="/css/likely.css">
</head>
<body>

<div class="container-fluid">

    <header class="row header-upd">
        <?php $this->widget('TopPanelWidget'); ?>
    </header>

    <div id="main-content" data-page="room" data-room_id="<?= ApiClient::getRoomInfo(Yii::app()->request->getParam('conf_id'))['room']['id'] ?>" class="main-content">

        <div class="col-sm-9 left-block">
            <?= preg_replace("/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?/", "$0?modestbranding=1&autoplay=1&rel=0&showinfo=0", $key_hangouts); ?>
            <div class="youtube-over"></div>
        </div>

        <div class="col-sm-3 banner-chat-wrap">
            <div class="row banners">
                <?php $this->widget('BannerWidget'); ?>
            </div>
            <div class="row">
                <div class="col-sm-12 chat-wrap">
                    <div class="row">
                        <div class="vk-comments">
                            <?php $this->widget('ChatWidget'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div style="display: none;">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72613717-1', 'auto');
        ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 933611509;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/933611509/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter34058645 = new Ya.Metrika({
                        id:34058645,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/34058645" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</div>

<script type="text/javascript" src="/js/front_main.js?v=1.0"></script>

<?= ThemeHelper::includeThemeStatic(); ?>
</body>


</html>