<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 22.01.16
 * Time: 14:07
 */

class RoomStateLog
{
    public static function log($room_id, $old_state, $new_state, $user_id = null)
    {
        $model = new RoomStateChangeLog();

        $model->room_id = $room_id;
        $model->old_state = $old_state;
        $model->new_state = $new_state;
        $model->date_change = date('Y-m-d H:i:s');
        $model->user_id = $user_id;

        $model->save();
    }
}