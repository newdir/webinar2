<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/css/schedule.css?v=1.0">

    <title>Расписание</title>
</head>
<body>

<div class="container">
    <header class="row header">
        <div class="col-xs-3 header-logo"></div>
        <div class="col-xs-5 hader-label">
            <p>Список вебинаров конференции <?= $conference['name'] ?></p>
        </div>
    </header>

    <?php if ($schedule): ?>

        <?php foreach ($schedule as $speaker): ?>

            <div class="row speaker-item">
                <div class="col-xs-2 webinar-date">
                    <p><?= Yii::app()->dateFormatter->format("dd MMMM",  strtotime($speaker['date'])) ?></p>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-1 webinar-time">
                            <p><?= Yii::app()->dateFormatter->format("HH:mm",  strtotime($speaker['date'])) ?></p>
                        </div>
                        <div class="col-xs-1 speaker-img">
                            <img src="<?= $speaker['img'] ?>" width="50" height="50">
                        </div>
                        <div class="col-xs-8">
                            <div class="row speaker-name">
                                <p><?= $speaker['name'] ?></p>
                            </div>
                            <div class="row speaker-theme">
                                <?php $link = isset($rooms[date('Y-m-d', strtotime($speaker['date']))])
                                    ? '/' . $conference['id'] . '/room/' . $rooms[date('Y-m-d', strtotime($speaker['date']))]
                                    : '#'; ?>
                                <a target="_blank" href="<?= $link ?>"><?= $speaker['theme'] ?></a>
                            </div>

                            <?php if (!empty($speaker['materials'])): ?>

                                <div class="row speaker-items">
                                    <p>Материалы</p>

                                    <?= BulletHtml::get($speaker['materials']) ?>

                                </div>

                            <?php endif; ?>

                            <?php
                            $date_open_items = new DateTime($speaker['date']);
                            $date_open_items = $date_open_items->setTime(23, 0, 0);
                            $timestamp_open_items = $date_open_items->getTimestamp();
                            ?>

                            <?php if (isset($items[$speaker['id']]) && date('U') >= $timestamp_open_items): ?>

                                <div class="row speaker-items">
                                    <p>Продукты</p>

                                    <?php foreach ($items[$speaker['id']] as $item): ?>

                                        <a target="_blank" href="http://newdirections.ru/form/?product_id=<?= $item['id'] ?>"><?= $item['label'] ?></a><br>

                                    <?php endforeach; ?>

                                </div>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    <?php endif ?>

</div>

</body>
</html>