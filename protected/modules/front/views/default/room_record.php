<div class="greeting-speaker">

    <?php if (! in_array($conference['type_id'], [ ConferenceHelper::CONFERENCE_TYPE_CLOSE, ConferenceHelper::CONFERENCE_TYPE_OPEN ])): ?>

        <div class="row conference-name">
            <p><?= $room['number_in_folder'] ?>-й день конференции <?= $room['conference']; ?></p>
        </div>
        <div class="row conference-date">
            <p><?= $room['date'] ?> с <?= substr(reset($room['performances'])['date'], 11, 5) ?> (мск)</p>
        </div>

    <?php endif; ?>

    <?php if (!empty($room['performances'])): ?>
        <?php foreach ($room['performances'] as $key =>$performance): ?>
            <div class="row row-speaker">
                <div class="col-xs-2 conference-speaker">
                    <div class="row speaker-photo">
                        <div class="speaker-front-photo" style="background-image: url(<?= $performance['speaker_img'] ?>)"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 speaker-name">
                            <p><?= $performance['speaker_name']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 conference-info">
                    <div class="row">
                        <div class="col-xs-12 conference-info-date">
                            <div class="conference-time">
                                <p><?= $room['date'] ?> с <?= substr($performance['date'], 11, 5) ?></p>
                            </div>
                            <div class="conference-region">
                                <p>Время московское</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 speaker-theme">
                            <p><?= $performance['theme'] ?></p>
                        </div>
                    </div>

                    <div class="row record-link">

                        <?php if (!empty($performance['key_hangouts_parts'])): ?>

                            <?php foreach (explode(PHP_EOL, $performance['key_hangouts_parts']) as $key_part => $part): ?>
                                <a href="record/<?= $key ?>?part=<?= $key_part ?>" target="_blank" class="watch-record-btn">Смотреть (часть <?= $key_part + 1 ?>)</a>
                            <?php endforeach; ?>

                        <?php else: ?>
                            <a href="record/<?= $key ?>" target="_blank" class="watch-record-btn">Смотреть</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    <?php endif; ?>
</div>