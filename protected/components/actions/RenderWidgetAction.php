<?php

/**
 * Класс для экшна, который должен лишь вывести указанный виджет
 * Class CommonAction
 */
class RenderWidgetAction extends CAction
{
    public $widget_name = null;

    public function run()
    {
        if ($this->widget_name) {
            //$this->widget($this->widget_name);

            $widget = new $this->widget_name;
            $widget->run();
        }
    }
}
