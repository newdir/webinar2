<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 13.08.15
 * Time: 12:42
 */

class GetBannersWidget extends CWidget {

    public function run()
    {
        $banners = ApiClient::getBannersList(Yii::app()->request->getParam('id'));

        echo (empty($banners)) ? '[]' : CJSON::encode($banners);
    }
}