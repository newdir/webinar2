/**
 * Created by artyomnorin on 20.08.15.
 */

if(!String.linkify) {
    String.prototype.linkify = function() {

        // http://, https://, ftp://
        var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;

        // www. sans http:// or https://
        var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

        // Email addresses
        var emailAddressPattern = /[\w.]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim;

        return this
            .replace(urlPattern, '<a target="_blank" href="$&">$&</a>')
            .replace(pseudoUrlPattern, '$1<a href="http://$2">$2</a>')
            .replace(emailAddressPattern, '<a href="mailto:$&">$&</a>');
    };
}

$(function () {

    function redrawRoom () {
        var header_height = $('header').height();
        var body_height = $(window).height();

        var delta_height = body_height - header_height;

        $('.left-block').css('min-height', delta_height + 'px');
        $('.left-block > iframe').css('height', delta_height + 'px');
    }

    var redrawBanner = function (){
        var banner = $('.banners').find('.main-banner');
        var banner_wight = banner.width();
        var banner_height = (banner_wight * 125) / 486;

        banner.css('height', banner_height + 'px');
    };

    if ($.cookie('webinar_nick')){
        $('.send_form').css('visibility', 'visible');
        $('.logout').css('visibility', 'visible');
        $('.reg_wrap').css('visibility', 'hidden');
    }

    $('.registration').submit(function(e){
        e.preventDefault();

        var data = {
            'user_data': {
                'nick':  $(this).find('.nick').val(),
                'email': $(this).find('.email').val().toLowerCase(),
                'pass':  $(this).find('.pass').val(),
                'url':   window.location.toString()
            }
        };

        var send_to = 'http://wbnr.pn-group.ru:3000/user/pre_reg';
        $.getJSON(send_to, data, function(json){
            if (parseInt(json.user_status) === 1){
                $('.reg_wrap').css('visibility', 'hidden');
                alert('Регистрация почти завершина, проверьте почту.');
            } else {
                alert('Данные не корректны или возможно email или ник заняты.');
            }
        });
    });

    $('.authentication').submit(function(e){
        e.preventDefault();

        var data = {
            'user_data': {
                'email': $(this).find('.email').val().toLowerCase(),
                'pass':  $(this).find('.pass').val()
            }
        };

        var send_to = 'http://wbnr.pn-group.ru:3000/user/login';
        $.getJSON(send_to, data, function(json){
            if (parseInt(json.user_data.status) === 1){
                $.cookie('webinar_nick', json.user_data.nick);
                $.cookie('webinar_pass', json.user_data.pass);
                $.cookie('webinar_email', json.user_data.email);

                $('.reg_wrap').css('visibility', 'hidden');
                $('.b-forget').css('visibility', 'hidden');
                $('.send_form').css('visibility', 'visible');
                $('.logout').css('visibility', 'visible');
                alert('Авторизация завершена');
            } else {
                alert('Пользователь не найден');
            }
        });
    });

    $('a.logout').click(function(e){
        e.preventDefault();

        $.removeCookie('webinar_nick');
        $.removeCookie('webinar_pass');
        $.removeCookie('webinar_email');
        location.reload();
    });

    $('.forget').submit(function(e){
        e.preventDefault();

        var data = {
            'email': $(this).find('.email').val()
        };

        var send_to = 'http://wbnr.pn-group.ru:3000/user/forget';
        $.getJSON(send_to, data, function(json){
            if (parseInt(json.user_data.status) === 1){
                alert('Данные отправлены на почту');
                $('.forget').css('visibility', 'hidden');
            } else {
                alert('Пользователь с таким email не найден');
            }
        });
    });

    $(document).ready(function () {
        $(window).resize(function (){
            redrawRoom();
            redrawBanner();
        }).trigger('resize');
    });

    $('.fancybox-image-container').fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',

        helpers : {
            title : {
                type : 'inside'
            }
        }
    });

    $('html').on('click', '.refresh-button', function (event) {

        event.preventDefault();
        location.reload();
    });

    $('body').on('click', '.webinar-material', function () {

        if($('.webinar-meterial-block').is(':visible')) {
            $('.webinar-meterial-block').hide();
        } else {
            $('.webinar-meterial-block').show();
        }

    });
});