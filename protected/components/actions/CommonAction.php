<?php

/**
 * Класс для экшна, который должен лишь выполнить  метод модели и вернуть результат
 * Class CommonAction
 */
class CommonAction extends CAction
{
    public function run()
    {
        $model_class = $this->getController()->model_class;
        $method = new $model_class;
        $res = $model->$method();
        echo CJSON::encode($res);
    }
}
