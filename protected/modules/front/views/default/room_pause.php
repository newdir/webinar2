<div class="main-content-10minbreak">
    <div class="content-10minbreak">
        <img src="/img/10minbreak-icon.png">
        <p class="content-10minbreak-title">Перерыв на 10 минут</p>
        <p>Спикеру нужно немного времени, чтобы отдохнуть. Отдохните и вы.</p>
    </div>
</div>