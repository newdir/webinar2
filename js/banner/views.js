/**
 * Created by artyomnorin on 13.08.15.
 */

App.Views.BannerView = Backbone.View.extend({

    className: 'content-banner',

    template: _.template($('#banner-show').html()),

    events: {
        'mouseenter' : 'showBannerAction',
        'mouseleave' : 'hideBannerAction',
        'click .content-info-delete' : 'deleteBanner',
        'click .content-info-edit' : 'editBanner'
    },

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.find('.wrap-banner').css('background-image', 'url(' + this.model.get('image') + ')');
        this.$el.find('.banner-text').css('color', this.model.get('color_label'));
        this.$el.find('.btn-warning').attr('href', this.model.get('url'));

        if(this.model.get('shadow') == 1){
            this.$el.find('.banner-text').css('text-shadow', '1px 1px 2px black');
        }

        return this;
    },

    showBannerAction: function () {
        this.$el.find('.banner-action').css('display', 'block');
    },

    hideBannerAction: function () {
        this.$el.find('.banner-action').css('display', 'none');
    },

    deleteBanner: function () {
        var that = this;

        if(confirm('Удалить баннер?')){
            this.model.destroy({
                success: function (model, response, options) {
                    that.remove();
                },
                error: function (model, xhr, options) {
                    alert('Ошибка при удалении баннера! Обратитесь в поддрежку.')
                }});
        }
    },

    editBanner: function () {
        var banner_edit_view = new App.Views.BannerEditView({model: this.model});
        this.$el.replaceWith(banner_edit_view.el);
        this.remove();
    }
});

App.Views.BannerCollectionView = Backbone.View.extend({

    el: '#all-banners',

    initialize: function () {
        this.render();
        EventManager.on('bannerSaved', this.addModel, this);
    },

    render: function () {
        this.collection.each(this.addModel, this);
        return this;
    },

    addModel: function(banner) {
        this.collection.add(banner);
        var banner_view = new App.Views.BannerView({model: banner});
        this.$el.find('.add-banner').before(banner_view.el);
    }
});

App.Views.AddBannerView = Backbone.View.extend({

    className: 'content-edit add-banner',

    template: _.template($('#add-banner').html()),

    events: {
        'submit .form-add-banner' : 'saveBanner',
        'click .btn-color' : 'changeColor',
        'click .btn-action' : 'changeAction'
    },

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template);
        $('.btn-add-banner').before(this.el);
        this.$el.imgInspector(1, '.wrap-new-banner');
        return this;
    },

    saveBanner: function (event) {
        event.preventDefault();

        var that = this;
        var regexp = new RegExp('^(https?:\/\/[^\\s]+)$');

        var form_data = Backbone.Syphon.serialize($(event.currentTarget));

        if (!regexp.test(form_data['url'].trim()) && form_data['url'].trim() !== '#') {
            alert('Проверьте ссылку баннера на корректность');
            return;
        }

        var new_banner = new App.Models.Banner({
            label: form_data['label'].trim(),
            url: form_data['url'].trim(),
            shadow: form_data['shadow'],
            image: form_data['image'].trim(),
            color_label: this.$el.find('.btn-color').filter('.active').val(),
            active: form_data['active'],
            button: this.$el.find('.btn-action').filter('.active').val() != 'null' ? this.$el.find('.btn-action').filter('.active').val() : null,
            output_type: form_data['output_type'],
            speaker_id: form_data['speaker_id']
        });

        new_banner.save(null, {
            success: function (model, response, options) {
                that.frontUpdateBanners(model.attributes);
                EventManager.trigger('bannerSaved', new App.Models.Banner(response));
                that.remove();
            },
            error: function (model, xhr, options) {
                alert('Ошибка при создании баннера! Обратитесь в поддержку');
            }
        });
    },

    frontUpdateBanners: function (banner) {
        changeData.status = 98;
        changeData.banner = banner;
        changeData.room_number = $('body').data('room-id');
        addKey(changeData);
        socket.emit('page change', JSON.stringify(changeData));
    },

    changeColor: function (button) {
        this.$el.find('.btn-color').removeClass('active');
        this.$el.find(button.toElement).addClass('active');
    },

    changeAction: function (button) {
        this.$el.find('.btn-action').removeClass('active');
        this.$el.find(button.toElement).addClass('active');
    }
});

App.Views.BannerEditView = Backbone.View.extend({

    className: 'edit-banner',

    template: _.template($('#add-banner').html()),

    events: {
        'submit .form-add-banner' : 'saveBanner',
        'click .btn-color' : 'changeColor',
        'click .btn-action' : 'changeAction'
    },

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template);
        $('.btn-add-banner').before(this.el);
        this.$el.imgInspector(1, '.wrap-new-banner');
        Backbone.Syphon.deserialize(this, this.model.toJSON());
        this.$el.find('.new-banner').attr('src', this.model.get('image'));
        this.$el.find('.btn-color').removeClass('active');
        this.$el.find('.btn-action').removeClass('active');
        this.$el.find('[value = "' + this.model.get('color_label') + '" ]').addClass('active');
        this.$el.find('[value = "' + this.model.get('button') + '" ]').addClass('active');
        this.$el.find('.banner-output :input[value="' + this.model.get('output_type') + '"]').attr('selected', 'selected');
        this.$el.find('.banner-speaker-id :input[value="' + this.model.get('speaker_id') + '"]').attr('selected', 'selected');
        return this;
    },

    saveBanner: function (event) {
        event.preventDefault();

        var that = this;
        var regexp = new RegExp('^(https?:\/\/[^\\s]+)$');

        var form_data = Backbone.Syphon.serialize($(event.currentTarget));

        if (!regexp.test(form_data['url'].trim()) && form_data['url'].trim() !== '#') {
            alert('Проверьте ссылку баннера на корректность');
            return;
        }

        var banner_info = {
            label: form_data['label'].trim(),
            url: form_data['url'].trim(),
            shadow: form_data['shadow'],
            image: form_data['image'].trim(),
            color_label: that.$el.find('.btn-color').filter('.active').val(),
            active: form_data['active'],
            button: this.$el.find('.btn-action').filter('.active').val() != 'null' ? this.$el.find('.btn-action').filter('.active').val() : null,
            output_type: form_data['output_type'],
            speaker_id: form_data['speaker_id']
        };

        this.model.save(banner_info, {
            success: function (model, response, options) {
                var banner_view = new App.Views.BannerView({model: new App.Models.Banner(response)});

                that.frontUpdateBanners(model.attributes);

                that.$el.replaceWith(banner_view.el);
                that.remove();
            },
            error: function (model, xhr, options) {
                $.ajax({
                    url: 'error/',
                    method: 'POST',
                    data: JSON.stringify({model: model, xhr: xhr, options: options})
                });
                alert('Ошибка при редактировании баннера! Обратитесь в поддержку');
            }
        });
    },

    frontUpdateBanners: function (banner) {
        changeData.status = 98;
        changeData.banner = banner;
        changeData.room_number = $('body').data('room-id');
        changeData.conference_id = $('body').data('conference_id');
        addKey(changeData);
        socket.emit('page change', JSON.stringify(changeData));
    },

    changeColor: function (button) {
        this.$el.find('.btn-color').removeClass('active');
        this.$el.find(button.toElement).addClass('active');
    },

    changeAction: function (button) {
        this.$el.find('.btn-action').removeClass('active');
        this.$el.find(button.toElement).addClass('active');
    }
});