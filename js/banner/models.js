/**
 * Created by artyomnorin on 13.08.15.
 */

App.Models.Banner = Backbone.Model.extend({

    idAttribute: 'id',

    initialize: function () {
        this.listenTo(this, 'invalid', function (model, error) {
            alert(error);
        });
    },

    sync: function (method, model, options) {

        switch (method) {
            case 'delete':
                Backbone.Model.prototype.sync.call(this, 'delete', model, options);
                break;
            default:
                Backbone.Model.prototype.sync.call(this, 'create', model, options);
        }
    },

    validate: function (attrs) {
        if(attrs.link === ''){
            return 'Укажите ссылку баннера';
        }

        if(attrs.image === ''){
            return 'Укажите изображение для баннера';
        }

        if(attrs.output_type === ''){
            return 'Укажите тип вывода баннера';
        }
    },

    base_url: 'banner/',

    url: function () {
        return (this.get('id') !== undefined) ? this.base_url + this.get('id')  + '/': this.base_url;
    }
});

App.Collections.BannerCollection = Backbone.Collection.extend({

    model: App.Models.Banner,

    url: 'banners/'
});