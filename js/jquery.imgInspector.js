(function ($){
    /**
     * param Integer max_count
     * param If max_count = 1, for img wrap
     */
    $.fn.imgInspector = function(max_count, main_image)
    {
        if (!max_count){
            max_count  = 0;
        }

        if (!main_image){
            main_image = null;
        }

        var self = this,
            form_data,
            hidden_input,
            file_data,
            last_image_src,
            count   = 0,
            img_src = '/img/',
            send_to = 'https://static.newdirections.ru/core/request_reception.php',
            origin  = 'http://local.dev:8080/';

        var regexp = new RegExp('/m', 'g');
        var new_imgs_path = '';


        hidden_input = '<input id="IISHI" hidden="hidden" type="file" />';

        self.find(main_image).append(hidden_input);

        hidden_input = self.find('#IISHI');

        $('.review-image').click(function () {
            self.find(hidden_input).click();
        });

        $(hidden_input).change(function(){
            file_data = $(this).prop('files')[0];

            form_data = new FormData();
            form_data.append('file', file_data);

            sendFile(form_data);
        });



        function sendFile (form_data)
        {
            preloadAction();

            $.ajax({
                url:    send_to,
                data:   form_data,
                type:   'POST',
                origin: origin,

                crossDomain: true,
                contentType: false,
                processData: false,

                success: function (data)
                {
                    successAction(data);
                },


                error: function (jqXHR, textStatus, errorThrown)
                {
                    errorAction(errorThrown);
                }
            });
        }


        function preloadAction ()
        {
            if (max_count == 1 && main_image){
                last_image_src = self.find('img').attr('src');

                $(self).find('img').attr({'src': img_src + 'loading.gif'});
            }
        }


        /** @namespace data.img_path */
        function successAction (data)
        {
            if (data.error){
                changeImageBack();

                alert(data.error);
            } else {
                if (max_count == 1 && main_image){

                    self.find('.new-banner').attr('src', data.img_path);
                    self.find('.img-path').val('');
                    self.find('.img-path').val(data.img_path.replace('/m', ''));
                } else if (max_count > 1){

                    if(self.find('.image-wrap').find('img').length > 0 && count == 0){
                        self.find('.image-wrap').find('img').remove();
                    }

                    count++;

                    self.find('.image-wrap').append('<img src="' + data.img_path + '" class="IPII_list" />');
                    new_imgs_path += ',' + data.img_path;
                    self.find('.img-path').val(new_imgs_path.substr(1).replace(regexp, ''));

                    if (count >= max_count){
                        self.find('button').prop('disabled', true);
                    }
                } else {
                    self.before('<img src="' + data.img_path + '" class="IPII_list" />');
                    new_imgs_path += ',' + data.img_path;
                    self.find('.img-path').val(new_imgs_path.substr(1).replace(regexp, ''));
                }
            }
        }


        function errorAction (errorThrown)
        {
            changeImageBack();

            alert(errorThrown);
        }


        function changeImageBack (src)
        {
            if (!src){
                if (last_image_src){
                    self.find('img').attr({'src': last_image_src});
                } else {
                    self.find('img').attr({'src': 'noimage.png'});
                }
            } else {

            }
        }
    };
})(jQuery);
