<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 05.04.16
 * Time: 8:26
 */

class PrivateRoomAccessFilter extends CFilter
{
    public $url_referrer;

    public $cookie_expire;


    public function preFilter($filter_chain)
    {
        $private_room_id = Yii::app()->request->getParam('id');
        $filter_chain->controller->redirect('https://newdirections.ru/webinars/room/' . $private_room_id . '?' . http_build_query(['hash' => Yii::app()->request->getParam('hash')]));

        $room = ApiClient::getRoomInfo($private_room_id);

        if (!$room['room']['is_private'] || !Yii::app()->user->isGuest) {

            return true;
        }

        $conference_id = Yii::app()->request->getParam('conf_id');
        $private_room_id = Yii::app()->request->getParam('id');
        $hash = Yii::app()->request->getParam('hash');

        if (
            !is_null($hash)
            && $hash === md5($conference_id . $private_room_id . '_rmprvt')
            && !isset(Yii::app()->request->cookies['private_room'])
        ) {
            return true;
        }

        if (isset(Yii::app()->request->cookies['private_room'])) {

            $hash = md5('room' . $conference_id . $private_room_id . 'private');

            if (Yii::app()->request->cookies['private_room']->value === $hash) {

                return true;
            }
        }

        $referrer = parse_url(Yii::app()->request->urlReferrer, PHP_URL_HOST);

        if ($referrer !== $this->url_referrer) {

            throw new CHttpException(403, 'Доступ должен быть совершен из личного кабинета.');
        }

        $cookie = new CHttpCookie('private_room', md5('room' . $conference_id . $private_room_id . 'private'));
        $cookie->configure([
            'httpOnly' => true,
            'path' => Yii::app()->request->url,
            'expire' => time() + $this->cookie_expire,
        ]);

        Yii::app()->request->cookies['private_room'] = $cookie;

        return true;
    }
}