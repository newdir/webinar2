<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<!--<html>-->
<html class="ua_js_yes">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WHD6Z8V');</script>
    <!-- End Google Tag Manager -->
    <title>Вебинар-сервис</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <script type="text/javascript" src="/js/vendors/jquery/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/js/vendors/underscore-min.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.css">

    <script type="text/javascript" src="/js/vendors/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="/js/vendors/bootstrap/bootstrap.min.js"></script>

    <script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea"
        });
    </script>

    <script type="text/javascript" src="/js/main.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/main.css?v=1.5">
    <link rel="stylesheet" type="text/css" href="/css/correction-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/css-fonts.css?v=1.0">

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHD6Z8V"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="container">

    <div class="header">
        <div class="row">
            <div class="col-xs-3" style="padding-left: 30px;">
                <div class="logo">
                    <div class="logo-base"><img src="/img/logo.png"></div>
                    <div class="logo-text">
                        <p class="foreven">Новые Направления</p>
                        <p class="organization-of-international-conferences">Организация международных конференций</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 col-xs-offset-6">
                <div class="logout">

                    <?php $this->widget('application.modules.back.widgets.TopPanelWidget'); ?>

                    <div class="logout-exit">
                        <p><a href="">выход</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-add">
        <p class="content-add-conf">Создать комнату</p>
        <?= CHtml::beginForm('', 'post', ['class' => 'form-horizontal', 'style' => 'margin-top: 30px;']) ?>
        <div class="form-group">
            <?= CHtml::label('Название', '', ['class' => 'col-xs-2 control-label']) ?>
            <div class="col-xs-8" style="padding-left: 60px;">
                <?= CHtml::textField('Room[name]', '', ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <?= CHtml::checkBox('Room[is_private]') ?>
                        Закрытая комната
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?= CHtml::label('Дата проведения', '', ['class' => 'col-xs-2 control-label']) ?>
            <div class="col-xs-3" style="padding-left: 60px;">
                <?= CHtml::dateField('Room[date]', '', ['class' => 'form-control room-date-select']) ?>
            </div>
        </div>

        <div class="form-group">
            <?= CHtml::label('Статус комнаты', '', ['class' => 'col-xs-2 control-label']) ?>
            <div class="col-xs-3" style="padding-left: 60px;">
                <?= CHtml::dropDownList('Room[state_id]', 0, array(0 => 'Закрыт', 1 => 'Приветствие', 2 => 'Трансляция', 3 => 'Перерыв на 10 минут', 4 => 'Техпроблемы', 5 => 'Продажа аудиозаписей'),
                    array('class' => 'form-control')) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10"  style="padding-left: 60px";>
                <button type="submit" class="btn btn-default btn-with-load-speaker">Сохранить</button>
            </div>
        </div>

    </div>
</div>
</body>
</html>