<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 12.11.15
 * Time: 12:04
 */

class ChangeLogHelper {

    public static function getChangedAttributes($old_attributes, $new_attributes){

        $changed_attributes = '';

        foreach ($new_attributes as $attr => $value) {

            if($old_attributes[$attr] != $value){
                $changed_attributes .= $attr . '=>' . $value . PHP_EOL;
            }
        }

        return $changed_attributes;
    }

}