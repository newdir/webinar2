<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 30.07.15
 * Time: 9:45
 *
 * Виджет для формирования JSON - объекта из списка спикеров для конкретной комнаты
 */

class GetSpeakersWidget extends CWidget {

    public function run()
    {
        $conf_id = Yii::app()->request->getParam('conf_id');
        $room_id = Yii::app()->request->getParam('id');

        $room = ApiClient::getAdminRoom($conf_id, $room_id);

        $speakers = $room['performances'];

        $this->render('getSpeakersWidget', ['speakers' => $speakers]);
    }
}