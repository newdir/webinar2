<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 12.11.15
 * Time: 11:42
 */

class ChangeLogBehavior extends CActiveRecordBehavior {

    protected $action = null;
    protected $old_attributes = array();
    protected $new_attributes = array();
    protected $changed_attributes = '';


    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';


    public function afterSave($event){

        if($this->owner->isNewRecord){

            $this->action = self::ACTION_CREATE;
            array_walk($this->owner->getAttributes(), function ($attribute, $key) {
                $this->changed_attributes .= $key . '=>' . $attribute . PHP_EOL;
            });
        }

        if(!$this->owner->isNewRecord){

            $this->new_attributes = $this->owner->getAttributes();
            $this->action = self::ACTION_UPDATE;
            $this->changed_attributes = ChangeLogHelper::getChangedAttributes($this->old_attributes, $this->new_attributes);
        }

        $this->saveLog();
    }

    public function afterFind($event){

        $this->old_attributes = $this->owner->getAttributes();
    }

    public function afterDelete($event){

        $this->action = self::ACTION_DELETE;

        $this->saveLog();

    }

    protected function saveLog(){

        $change_log = new ChangeLog();

        $change_log->date_change = date('Y-m-d H:i:s');
        $change_log->user_id = Yii::app()->user->id;
        $change_log->new_value = empty($this->changed_attributes) ? null : $this->changed_attributes;
        $change_log->materail_type = $this->owner->tableName();
        $change_log->material_id = $this->owner->id;
        $change_log->ip = ip2long(Yii::app()->request->getUserHostAddress());
        $change_log->action = $this->action;

        if (!$change_log->save()) {
            Yii::log('BANNER_LOG: ' . serialize($change_log->getErrors()), CLogger::LEVEL_ERROR);
        }
    }
}