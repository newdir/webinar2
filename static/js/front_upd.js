$('document').ready(function () {
    
    function redrawRoom () {
        var header_height = $('header').height();
        var body_height = $(window).height();
        
        console.log(header_height, body_height);
        
        var delta_height = body_height - header_height;
        
        $('.left-block').css('min-height', delta_height + 'px');
        $('.left-block > iframe').css('height', delta_height + 'px');
    }
    
    $(window).resize(function () {
        redrawRoom();
    }).trigger('resize');
});