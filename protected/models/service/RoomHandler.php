<?php

class RoomHandler
{

    const ROOM_STATE_CLOSE = 0;
    const ROOM_STATE_GREETING = 1;
    const ROOM_STATE_STREAM_1 = 2;
    const ROOM_STATE_STREAM_2 = 21;
    const ROOM_STATE_STREAM_3 = 22;
    const ROOM_STATE_TECH_PAUSE = 3;
    const ROOM_STATE_TROUBLE = 4;
    const ROOM_STATE_BUY_RECORDS = 5;
    const ROOM_STATE_VIEW_RECORDS = 6;
    const ROOM_STATE_POSTPONED = 7;
    const ROOM_STATE_MK_IS_OVER = 8;

    /**
     * Метод возвращает количество посетителей онлайн
     */
    public static function getOnlineCount()
    {
        $room_id = Yii::app()->request->getParam('id', null);
        //TODO закешировать на 1,5 минуты
    }

    /**
     * Метод возвращает набор состояний каждого из ресурсов
     * @return mixed
     */
    public static function getStates()
    {
        $room_id = Yii::app()->request->getParam('id', null);
        return Yii::app()->db->createCommand()
                   ->select('type, value')
                   ->from('room_state')
                   ->where('room_id=:room_id', array(':room_id' => $room_id))
                   ->queryAll();
    }
}