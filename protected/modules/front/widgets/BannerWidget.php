<?php

/**
 * Виджет выводит банеры привязанные к комнате
 * Class BannerWidget
 */
class BannerWidget extends CWidget
{
    public function run()
    {
        $conference_id = Yii::app()->request->getParam('conf_id');
        $room_id = Yii::app()->request->getParam('id');

        $banners = ApiClient::getBannersList($room_id);

        if (empty($banners)) {
            return;
        }

        $banners = array_filter($banners, function ($banner) {
            return $banner['active'];
        });

        

        $this->render('bannerWidget', array(
            'banners' => array_slice($banners, 0, 3),
            'conference_id' => $conference_id,
        ));
    }
} 