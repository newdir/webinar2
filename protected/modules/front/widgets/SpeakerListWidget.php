<?php

/**
 * Класс выводит список спикеров
 * Class SpeakerListWidget
 */
class SpeakerListWidget extends CWidget
{
    public function run()
    {
        $this->render('speakerListWidget');
    }
} 