<div class="btn-group btn-group-justified" role="group">
    <?php foreach($buttons as $value => $name): ?>

        <?= CHtml::openTag('div', array('class' => 'btn-group', 'role' => 'group')); ?>

            <?php if($room['state_id'] == $value): ?>
                <?= CHtml::htmlButton($name, array( 'class' => 'btn btn-default change-state-room active',
                                                    'value' => $value)); ?>
                <?= CHtml::closeTag('div'); ?>
                <?php continue; ?>
            <?php endif; ?>

            <?= CHtml::htmlButton($name, array( 'class' => 'btn btn-default change-state-room',
                                                'value' => $value)); ?>

        <?= CHtml::closeTag('div'); ?>

    <?php endforeach; ?>

    <div class="btn-group" role="group">
        <button class="btn btn-default update-front-btn" value="99" name="yt0" type="button">Обновить страницы зрителей</button>
    </div>
</div>