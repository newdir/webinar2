<?php

/**
 * Виджет выводит блок количества посетителей онлайн
 * Class OnlineCountWidget
 */
class OnlineCountWidget extends CWidget
{
    public function run()
    {
        $path = $this->getController()->static_path;
        Yii::app()->clientScript->registerScriptFile($path . '/js/online_count.js',CClientScript::POS_END);
        $this->render('onlineCountWidget');
    }
} 