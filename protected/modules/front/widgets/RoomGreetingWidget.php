<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 22.01.16
 * Time: 14:41
 */

class RoomGreetingWidget extends CWidget
{
    public $room = null;

    public $conference = null;

    public $speakers = null;

    public $render_template = null;


    public function run()
    {
        if (empty($this->speakers)) {
            return;
        }

        if (!isset($this->render_template)) {
            $this->render_template = 'roomGreeting';
        }

        $this->render($this->render_template . 'Widget', [
            'room' => $this->room,
            'conference' => $this->conference,
            'speakers' => $this->speakers,
        ]);
    }
}