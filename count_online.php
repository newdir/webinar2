<?php

const TIMELIFE_COUNT_ONLINE = 60;
const CACHE_KEY = 'online_room_';

validateRequest();

$room_id = $_GET['room_id'];

header('Content-Type: application/json');
echo json_encode(['success' => true, 'count' => getCount($room_id)]);


function validateRequest()
{
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
        header("HTTP/1.x 404 Not Found");
        exit;
    }

    if (!isset($_GET['room_id'])){
        header("HTTP/1.x 404 Bad request");
        exit;
    }
}

function getCount($room_id)
{
    $cache_key = CACHE_KEY . $room_id;

    $credentials = getConnectionCredentials();

    try{
        $pdo = new PDO($credentials['string'], $credentials['username'], $credentials['password'], [PDO::ATTR_PERSISTENT]);

    }catch (PDOException $e){
        return 5;
    }

    setUserOnline($pdo, $room_id);

    $value = apc_fetch($cache_key);
    $pseudo_online = (apc_exists('pseudo_online_' . $room_id)) ? apc_fetch('pseudo_online_' . $room_id) : 0;

    if($value===false)
    {
        $statement = $pdo->prepare('SELECT count(1) as count FROM clientonline
          WHERE room_id = :room_id AND updated > UNIX_TIMESTAMP() - ' . round(TIMELIFE_COUNT_ONLINE * 1.5));
        $statement->bindParam(':room_id', $room_id, PDO::PARAM_INT);
        $statement->execute();
        $value = $statement->fetch(PDO::FETCH_ASSOC)['count'];

        apc_store($cache_key, $value, TIMELIFE_COUNT_ONLINE);
    }
    return $value + $pseudo_online;
}

function setUserOnline(PDO $pdo, $room_id)
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $ua=( isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';

    $time_stamp = time();

    $statement = $pdo->prepare('INSERT INTO clientonline(user_hash,room_id,updated) VALUES (:user_hash, :room_id, :updated)
				ON DUPLICATE KEY UPDATE updated=:time_stamp');

    $statement->bindParam(':user_hash', md5($ip . $ua));
    $statement->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $statement->bindParam(':updated', $time_stamp, PDO::PARAM_INT);
    $statement->bindParam(':time_stamp', $time_stamp, PDO::PARAM_INT);

    $statement->execute();
}

function getConnectionCredentials()
{
    $connection = apc_fetch('connection');

    if (!$connection) {
        $cofig = require_once 'protected/config/main.php';

        $connection['string'] = $cofig['components']['db']['connectionString'];
        $connection['username'] = $cofig['components']['db']['username'];
        $connection['password'] = $cofig['components']['db']['password'];

        apc_store('connection_string', $connection, TIMELIFE_COUNT_ONLINE);
    }

    return $connection;
}