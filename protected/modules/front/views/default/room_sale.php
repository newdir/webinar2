<div class="main-content-info">
    <div class="content-info">
        <img src="<?= $conference['images']; ?>">
        <div class="info-cost">
            <p>Все записи конференции</p>
            <p class="info-title"><b><?= $conference['name'] ?></b></p>
            <div>
                <div class="info-price item-1">
                    <img src="/img/info-newPrice.png">
                    <p><?= $conference['price'] ?></p>
                </div>
                <div class="info-price old-price">
                    <img src="/img/info-oldPrice.png">
                    <p><?= $conference['price_old'] ?></p>
                </div>
            </div>
            <div>
                <p class="info-text">
                    <?= $conference['label'] ?>
                </p>
            </div>
            <div class="info-btn">
                <a class="btn" target="_blank" href="<?= $conference['url']; ?>">Купить</a>
            </div>
        </div>
    </div>
</div>