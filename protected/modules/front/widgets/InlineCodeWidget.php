<?php

/**
 * Виджет выводит код внешних сервисов (метрика, ретаргетинг и тд)
 * Class InlineCodeWidget
 */
class InlineCodeWidget extends CWidget
{
    public function run()
    {
        $this->render('inlineCodeWidget');
    }
} 