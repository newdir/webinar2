<script type="text/template" id="header-info">
    <div class="col-sm-1 header-logo">
        <img src="/img/logo-header_upd.png" class="img-responsive center-block logo-webinar">
    </div>
    <div class="col-sm-2 header-conference">
        <span class="header-text"><%= folder.name %></span>
    </div>
    <div class="col-sm-2 header-speakers">

        <?php if ($room['is_private'] != 1): ?>
            <ul>
                <% speakers.forEach(function (speaker) { %>

                <li><span><%= speaker.date_perfomance.substr(11, 5) %></span>&nbsp;&nbsp;<%= speaker.name %></li>

                <% }) %>
            </ul>
        <?php endif; ?>

    </div>
    <div class="col-sm-1 header-schedule">

        <?php if ($room['is_private'] != 1
                && ! in_array($conference['type_id'], [ ConferenceHelper::CONFERENCE_TYPE_CLOSE, ConferenceHelper::CONFERENCE_TYPE_OPEN ])): ?>

            <a target="_blank" href="<%= '/conference/' + folder.id + '/schedule' %>">Расписание вебинаров</a>

        <?php endif; ?>

    </div>

    <div class="col-sm-1 likely likely-small likely-light">

        <?php if (!$is_watch_page && $room['is_private'] != 1
                && ! in_array($conference['type_id'], [ ConferenceHelper::CONFERENCE_TYPE_CLOSE, ConferenceHelper::CONFERENCE_TYPE_OPEN ])): ?>

            <div class="vkontakte"></div>
            <div class="twitter"></div>
            <div class="facebook"></div>

        <?php endif; ?>

    </div>

    <div class="col-sm-2">
        <div class="row">
            <a class="col-sm-6 refresh-button" href="#">Обновить</a>
        </div>
        <?php if (!$is_watch_page && $room['is_private'] != 1): ?>
            <div class="row">
                <p class="header-audience">Зрителей онлайн: <?= $count_online ?></p>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-sm-1 tech-trouble">

        <?php if ($room['is_private'] != 1): ?>

            <div class="row">
                <div class="col-sm-12">
                    <p class="webinar-material">Материалы для МК</p>
                </div>
            </div>

        <?php endif; ?>

    </div>

    <div class="col-sm-2 pc-client">
        <div class="row">
            <div class="col-sm-12 pc-client-btn-wrap">
                <a class="pc-client-button" href="http://client.newdirections.ru" target="_blank">Личный кабинет</a>
            </div>
        </div>
        <div class="row pc-client-hint">
            <div class="col-sm-12">
                <span>для клиентов</span>
            </div>
        </div>
    </div>

</script>