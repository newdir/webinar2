<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 29.12.15
 * Time: 14:15
 */

class ConferenceAccessControlFilter extends CFilter
{
    public function preFilter($filterChain)
    {
        $user = UserHelper::get();
        $conference_id = Yii::app()->request->getParam('conf_id');
        $user_role = ApiClient::onCheckRights($user, $conference_id);

        if (empty($user_role)) {
            throw new CHttpException(403, 'Access denied');
        }

        return true;
    }
}