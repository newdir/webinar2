<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    public function authenticate(){

        $user = ApiClient::onAuth($this->username);

        if(!$user['success']){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            return false;
        }

        if(!CPasswordHelper::verifyPassword($this->password, $user['password'])){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            return false;
        }

        $this->_id = $user['id'];
        $this->username = $user['nick_name'];

        $this->setState('id', $user['id']);
        $this->setState('access_level', $user['access_level']);
        $this->setState('nick_name', $user['nick_name']);
        $this->setState('email', $user['email']);

        $this->errorCode = self::ERROR_NONE;

        return true;
    }
}