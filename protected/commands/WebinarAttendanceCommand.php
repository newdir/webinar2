<?php

class WebinarAttendanceCommand extends CConsoleCommand
{
    const START_HOUR = 19;

    const START_MINUTE = 0;

    public function run($args)
    {
        $rooms =ApiClient::getRoomsToday();

        if (empty($rooms)) {
            return;
        }

        $current_datetime = new DateTimeImmutable();
        $prev_datetime = $current_datetime->sub(new DateInterval('P1D'))->setTime(self::START_HOUR, self::START_MINUTE);
        $datetime_start = $current_datetime->setTime(self::START_HOUR - 3, self::START_MINUTE);

        $webinar_attendance = [];

        foreach ($rooms as $room) {

            $room_date = new DateTimeImmutable($room['date']);
            $datetime_diff = $current_datetime->diff($prev_datetime);

            if (
                ($room_date == $current_datetime->setTime(0, 0) && $current_datetime > $datetime_start) ||
                ($room_date == $prev_datetime->setTime(0, 0) && $datetime_diff->h < 24)
            ) {
                $tmp['date'] = date('Y-m-d H:i:s');
                $tmp['conference_id'] = (string) $room['conference_id'];
                $tmp['id'] = (string) $room['id'];
                $tmp['count_viewers'] = (string) (new ARedisSet('room:' . $room['id'] . ':ips'))->count();

                $webinar_attendance[] = $tmp;
            }
        }

        if (!empty($webinar_attendance)) {
            ApiClient::saveWebinarAttendance($webinar_attendance);
        }
    }
}