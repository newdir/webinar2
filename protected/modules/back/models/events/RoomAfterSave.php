<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 01.07.15
 * Time: 17:19
 *
 * Событие для дополнения записи полями, коотрые не приходят с клиента при создании новой комнаты
 */

class RoomAfterSave extends CModelEvent
{
    public function setRoomAttribute()
    {
        $room = $this->sender;
        $conf_id = $this->params['conference_id'];

        $room->folder_id = $conf_id;
        $room->created = date('Y-m-d H:i:s');
    }
}