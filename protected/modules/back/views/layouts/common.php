<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<!--<html>-->
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WHD6Z8V');</script>
    <!-- End Google Tag Manager -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=CHtml::encode($this->pageTitle)?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <script type="text/javascript">
        var room_json = <?php $this->widget('application.modules.front.widgets.RoomJsonWidget'); ?>;
    </script>

    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="/css/main.css?v=1.7">
    <link rel="stylesheet" type="text/css" href="/css/correction-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/css-fonts.css?v=1.0">
    <link rel="stylesheet" type="text/css" href="/css/img_inspector.css">
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css">

    <script type="text/javascript">
        var preload_banner_collection = <?php $this->widget('application.modules.back.widgets.GetBannersWidget'); ?>;
    </script>

    <script type="text/javascript" src="/js/vendors/jquery/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/js/select2.min.js"></script>
    <script type="text/javascript" src="/js/main.js?v=1.4"></script>

    <script type="text/javascript" src="/js/vendors/underscore-min.js"></script>
    <script type="text/javascript" src="/js/vendors/backbone-min.js"></script>
    <script type="text/javascript" src="/js/vendors/backbone.syphon.min.js"></script>

</head>

<body data-room-id="<?= $room_id; ?>" data-conference_id="<?= $conf_id ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHD6Z8V"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="loading-speakers"></div>
<div class="container">
    <div class="header">
        <div class="row">
            <div class="col-xs-3" style="padding-left: 30px;">
                <div class="logo">
                    <div class="logo-base"><img src="/img/logo.png"></div>
                    <div class="logo-text">
                        <p class="foreven">Новые Направления</p>
                        <p class="organization-of-international-conferences">Организация международных конференций</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 col-xs-offset-6">
                <div class="logout">

                    <?php $this->widget('application.modules.back.widgets.TopPanelWidget'); ?>

                    <div class="logout-exit">
                        <p><a href="/logout/">выход</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="conference">
        <div class="row" style="background: #1f2831;">
            <div class="col-xs-7">
                <div class="row">
                    <div class="col-xs-2" style="padding-left: 30px;">
                        <p class="text-center">Конференция</p>
                    </div>
                    <div class="col-xs-5">

                        <?php $this->widget('application.modules.back.widgets.FolderListWidget'); ?>

                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-default btn-sm btn-clear-cache">Сбросить кэш</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu">
        <div class="menu-level_two">
            <div class="menu-level_two-rel">
                <div class="menu-level_two-main">
                    <div class="item-level_two-main">
                        <img src="/img/icon/icon_light_1.png">
                    </div>
                    <div class="item-level_two-main">
                        <img src="/img/icon/icon_light_2.png">
                    </div>
                    <div class="item-level_two-main" id="item-3">
                        <img src="/img/icon/icon_light_3.png">
                    </div>
                    <div class="item-level_two-main">
                        <img src="/img/icon/icon_light_4.png">
                    </div>
                    <div class="item-level_two-main">
                        <img src="/img/icon/icon_light_5.png">
                    </div>
                </div>
                <div class="menu-level_two-dropdown">
                    <?= CHtml::link('Добавить день', '/admin/'.Yii::app()->request->getParam('conf_id').'/room', array( 'class' => 'btn btn-add',
                        'style' => 'margin:10px;')); ?>

                    <?php $this->widget('application.modules.back.widgets.RoomListWidget'); ?>
                </div>
            </div>
        </div>
        <div class="menu-level_one">
            <div class="item-level_one">
                <p><img src="/img/icon/icon_white_1.png">&nbsp;&nbsp;&nbsp; Спикеры</p>
                <div class="item-numb"><span class="badge">25</span></div>
            </div>
            <div class="item-level_one" style="background: #ffffff; color:  #1f2831;">
                <p><img src="/img/icon/icon_dark_2.png">&nbsp;&nbsp;&nbsp; Продукты</p>
                <div class="item-numb"><span class="badge">213</span></div>
            </div>
            <div class="item-level_one">
                <p><img src="/img/icon/icon_white_3.png">&nbsp;&nbsp;&nbsp; Расписание</p>
            </div>
            <div class="item-level_one">
                <p><img src="/img/icon/icon_white_4.png">&nbsp;&nbsp;&nbsp; Заявки</p>
            </div>
            <div class="item-level_one">
                <p><img src="/img/icon/icon_white_5.png">&nbsp;&nbsp;&nbsp;&nbsp; Отчеты</p>
            </div>
            <div class="item-level_one">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Обратная связь</p>
            </div>
        </div>
    </div>

    <div class="content-main">

        <div class="content-room">
            <?= CHtml::beginForm('/admin/' . $conf_id . '/room/' . $room_id . '/', 'post', array('class' => 'form-horizontal')); ?>
            <div class="col-xs-12 room-status">

                <?php $this->widget('application.modules.back.widgets.StatusRoomWidget', array('room_model' => $model_room)); ?>

            </div>
            <div class="col-xs-9">
                <div class="form-group">
                    <label class="col-xs-2 control-label">Название</label>
                    <div class="col-xs-9 col-xs-offset-1">
                        <?= CHtml::textField('Room[name]', isset($model_room['name']) ? $model_room['name'] : '', ['class' => 'form-control']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label">Дата проведения</label>
                    <div class="col-xs-4 col-xs-offset-1">
                        <?= CHtml::dateField('Room[date]', isset($model_room['date']) ? date('Y-m-d', strtotime($model_room['date'])) : '', ['class' => 'form-control room-date-select']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label">Перенесено на</label>
                    <div class="col-xs-4 col-xs-offset-1">
                        <?= CHtml::dateTimeLocalField('Room[date_postpone]', !empty($model_room['date_postpone']) ? date('Y-m-d\TH:i', strtotime($model_room['date_postpone'])) : null, ['class' => 'form-control room-date-select']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label">Видео пока не готово</label>
                    <div class="col-xs-4 col-xs-offset-1">
                        <?= CHtml::checkBox('Room[video_not_ready]', isset($model_room['video_not_ready']) ? $model_room['video_not_ready'] : false, ['style' => 'margin-top: 15px']) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="content-links">
                    <div class="pull-left"><img src="/img/icon/link.png"></div>
                    <?php $this->widget('application.modules.back.widgets.PagesWidget'); ?>
                </div>
            </div>


            <div class="col-xs-12">
                <div class="col-xs-9">
                    <div class="form-group">
                        <label class="control-label col-xs-2">Накрутка пользователей онлайн</label>
                        <div class="col-xs-9 col-xs-offset-1">
                            <div class="row">
                                <div class="col-xs-1 col-xs-offset-3 pseudo_user_online_label">
                                    <p></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-1 pseudo_user_online_caption">
                                    <p>0</p>
                                </div>
                                <div class="col-xs-5">
                                    <input type="range" class="pseudo_user_online_range" max="400" min="0" value="<?= $pseudo_online ?>">
                                </div>
                                <div class="col-xs-1 pseudo_user_online_caption">
                                    <p><?= $pseudo_online ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-7">
                <div class="checkbox">
                    <div class="col-xs-10">
                        <label>
                            <?= CHtml::checkBox('Room[is_private]', isset($model_room['is_private']) ? $model_room['is_private'] : false) ?>
                            Закрытая комната
                        </label>
                    </div>
                </div>
            </div>
            <?php if ($model_room['is_private']): ?>
                <div class="col-xs-10 col-xs-offset-2">
                    Ссылка для зрителей, <strong>КОТОРЫЕ <span style="color: #ff0000">ОПЛАТИЛИ</span> ВЕБИНАР:</strong>
                    <a target="_blank"
                       href="https://newdirections.ru/webinars/room/<?= $room_id . '?hash=' . md5($conf_id . $room_id . '_rmprvt') ?>">
                        https://newdirections.ru/webinars/room/<?= $room_id . '?hash=' . md5($conf_id . $room_id . '_rmprvt') ?>
                    </a>
                </div>
            <?php endif; ?>

            <div class="col-xs-12">
                <div class="checkbox">
                    <div class="col-xs-8">
                        <label>
                            <?= CHtml::checkBox('Room[is_publish_vk_comments]', isset($model_room['is_publish_vk_comments']) ? $model_room['is_publish_vk_comments'] : false) ?>
                            Публиковать комментарий ВК на странице у автора комментария
                        </label>
                    </div>
                </div>
            </div>

            <input name="Room[speakers]" hidden="hidden" class="list-speakers">

            <script type="text/template" id="load-speaker-list">
                <div class="form-group load-speaker">
                    <div class="col-xs-offset-2" style="padding-left: 30px;">
                        <p><b>В комнату будут добавлены следующие спикеры:</b></p>
                    </div>
                    <% $.each(speakers, function (i, speaker) { %>
                    <div class="col-xs-1 col-xs-offset-2 wrap-img-circle" style="padding-left: 30px;">
                        <img class="img-circle" <%= 'src=' + speaker.img %> width="55" height="55">
                    </div>
                    <div class="col-xs-8 speaker-info">
                        <div class="form-group">
                            <div class="col-xs-6">
                                <p class="info-no-edit"><%= speaker.name %></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8">
                                <p class="title info-no-edit"><b><%= speaker.theme_perfomance %></b></p>
                            </div>
                        </div>
                    </div>
                    <% }); %>
                </div>
            </script>

            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-9" style="padding-left: 30px;">
                        <button type="submit" class="btn btn-default btn-with-load-speaker">Сохранить</button>
                    </div>
                </div>
            </div>
            <?= CHtml::endForm(); ?>

            <?php if (isset($model_room['date']) && !empty($model_room['date']) && !empty($model_room['performances'])): ?>

                <form method="post" class="form-horizontal" name="FormPerfomance" action="perfomance/">

                    <?php $counter=1; ?>
                    <?php foreach ($model_room['performances'] as $performance_id => $performance): ?>
                        <div class="col-xs-12">
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <label class="col-xs-2 control-label">Amazon #<?= $counter ?></label>
                                    <div class="col-xs-9 col-xs-offset-1">
                                        <input type="text" value="<?= $performance['amazon_link'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3" style="height: 44px"></div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <label class="col-xs-2 control-label">Key Hangouts #<?= $counter ?></label>
                                    <div class="col-xs-9 col-xs-offset-1 perfomance_inputs">
                                        <input type="text" value="<?= htmlspecialchars($performance['key_hangouts'], ENT_QUOTES | ENT_HTML5, 'UTF-8') ?>" class="form-control key-hangouts" name="FormPerfomance[<?= $performance_id ?>][key_hangouts]">
                                        <input type="text" value="<?= $performance_id ?>" class="perfomance_id" name="FormPerfomance[<?= $performance_id ?>][perfomance_id]" hidden="hidden">
                                        <p class="text-danger" style="margin: 0">Используйте это имя файла для сохранения видео на внешних ресурсах: <b><?= $room_id .  md5($conf_id . $room_id . 'number_' . $performance_id) ?></b></p>
                                        <p>Если выступление состоит из нескольких частей <a href="#" class="add_perf_parts">укажите</a> ссылки на части</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3"  style="height: 108px"></div>
                        </div>
                        <div class="col-xs-12" style="display: none">
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3">
                                        <label class="control-label">Части записей для трансляции #<?= $counter ?></label>
                                        <textarea class="form-control key-hangouts" name="FormPerfomance[<?= $performance_id ?>][key_hangouts_parts]"><?= htmlspecialchars($performance['key_hangouts_parts'], ENT_QUOTES | ENT_HTML5, 'UTF-8') ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3" style="height: 107px;"></div>
                        </div>
                        <?php $counter++; ?>

                    <?php endforeach; ?>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-offset-2 col-xs-9" style="padding-left: 30px;">
                                <button type="submit" class="btn btn-default btn-with-load-speaker">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </form>

            <?php endif; ?>

        </div>

        <div class="content-speakers" id="all-speakers">

            <div class="content-title">
                <p>Спикеры</p>
            </div>

            <?php $this->widget('application.modules.back.widgets.GetSpeakersWidget'); ?>

        </div>

        <div class="content-banners" id="all-banners">

            <div class="content-title">
                <p>Баннеры</p>
            </div>

            <script type="text/template" id="banner-show">
                <a target="_blank" href="<%= url %>" class="wrap-banner">
                    <div class="banner-text">
                        <p> <%= label %> </p>
                    </div>

                    <% if(button != '' && button != undefined && button != null) { %>
                    <button class="btn btn-warning banner-button" target="_blank"> <%= button %> </button>
                    <% } %>

                    <a href="<%= url %>" target="_blank"></a>
                    </div>
                    <div class="wrap-banner-action">
                        <div class="banner-action" style="display: none;">
                            <div class="content-info-edit" data-active="1">
                                <img src="/img/icon/sp-edit-active.png">
                            </div>
                            <div class="content-info-delete" data-active="0">
                                <img src="/img/icon/sp-delete.png">
                            </div>
                        </div>
                </a>
            </script>


            <script type="text/template" id="add-banner">
                <form class="col-xs-12 form-horizontal form-add-banner">
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Надпись</label>
                        <div class="col-xs-8 col-xs-offset-1">
                            <input type="text" class="form-control" name="label">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Цвет надписи</label>
                        <div class="col-xs-2 col-xs-offset-1">
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default active btn-color" value="fff">Белый</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-color" value="000">Черный</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="shadow"> Тень
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-5 form-help">
                            <p>Используйте белый цвет для темного фона и черный для светлого.</p>
                            <p>Если контраст недостаточен, включите тень.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Ссылка</label>
                        <div class="col-xs-8 col-xs-offset-1">
                            <input type="text" class="form-control" name="url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Кнопка</label>
                        <div class="col-xs-3 col-xs-offset-1">
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default active btn-action" value="Купить">Купить</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-action" value="Скачать">Скачать</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-action" value="Смотреть">Смотреть</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-action" value="Заказать">Заказать</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-action" value="null">Без кнопки</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Тип вывода</label>
                        <div class="col-xs-3 col-xs-offset-1">
                            <select class="form-control banner-output" name="output_type">
                                <option value="0">Стандарт</option>
                                <option value="1">4:3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-1 control-label">Спикер</label>
                        <div class="col-xs-3 col-xs-offset-1">
                            <select class="form-control banner-speaker-id" name="speaker_id">
                                <?php if (!empty($speakers)): ?>

                                    <?php foreach ($speakers as $name => $speaker_id): ?>

                                        <option value="<?= $speaker_id ?>"><?= $name ?></option>

                                    <?php endforeach; ?>

                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <!--<div class="col-xs-offset-2 new-banner"></div>
                    <img class="col-xs-offset-2 new-banner" src="">
                    <div class="review col-xs-2">
                        <button type="button" class="btn btn-default">Обзор</button>
                    </div>-->
                    <div class="row wrap-new-banner">
                        <img class="col-xs-2 col-xs-offset-2 new-banner" src="">
                        <button type="button" class="btn btn-default review-image review-banner">Обзор</button>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-2 show-banner-on-webinar">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="active">
                                    Показать на вебинаре
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-8">
                            <button type="submit" class="btn btn-default">Сохранить</button>
                        </div>
                    </div>

                    <input type="text" hidden="hidden" class="img-path" value="" name="image">
                </form>
                <div class="speaker-action">
                    <div class="content-info-edit" data-active="0">
                        <img src="/img/icon/sp-edit.png">
                    </div>
                    <div class="content-info-delete" data-active="0">
                        <img src="/img/icon/sp-delete.png">
                    </div>

                </div>
                <!--<div class="content-info-delete-box">
                    <p>Удалить баннер?</p>
                    <button type="button" class="btn btn-danger delete-box-btn">Да</button>
                    <button type="button" class="btn btn-default delete-box-btn">Нет</button>
                </div>-->
            </script>

            <div class="content-add-btn btn-add-banner">
                <button type="button" class="btn btn-default-add">Добавить баннер</button>
                <p>&nbsp;</p>
            </div>
        </div>

    </div>
</div>
<!--<script type="text/javascript" src="/js/main.js"></script>-->
<script type="text/javascript" src="/js/app.js"></script>

<script type="text/javascript" src="/js/banner/models.js?v=1.2"></script>
<script type="text/javascript" src="/js/banner/views.js?v=2.0"></script>
<script type="text/javascript" src="/js/banner/main.js?v=1.1"></script>

<script type="text/javascript" src="/js/jquery.imgInspector.js?v=1.1"></script>


<script src="/js/io/socket.io-1.4.5.js"></script>
<script src="/js/io/io-admin.js?v=1.4"></script>
</body>

</html>
