<?php

class SpeakerSheduleWidget extends CWidget
{
    public function run()
    {
        $conference_id = Yii::app()->request->getParam('id');

        $data = ApiClient::getSchedule($conference_id);

        $rooms = ArrayToAssoc::filter(ApiClient::getRoomList($conference_id), 'id', 'date');

        $this->render('speakerSheduleWidget', [
            'schedule' => $data['schedule'],
            'conference' => $data['conference'],
            'items' => $data['items'],
            'rooms' => $rooms,
        ]);
    }
} 