<?php

/**
 * Хелпер выполняет преобразование многострочного текста в html-ненумерованный список
 * Class BulletHtml
 */
class BulletHtml {

    public static function get($text_from_textarea)
    {
        $array_of_string_separated_by_n = explode("\n", $text_from_textarea);

        $string_list = CHtml::openTag('ul');

        foreach ($array_of_string_separated_by_n as $string) {

            $string = self::_wrapLinks($string);

            $string_list .= CHtml::openTag('li');
            $string_list .= trim($string, " \t\r\0\x0B");
            $string_list .= CHtml::closeTag('li');
        }

        $string_list .= CHtml::closeTag('ul');

        return $string_list;
    }

    public static function _wrapLinks($text)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,}(\/\S*)?/";
        $text = preg_replace($reg_exUrl, "<br><a target='_blank' href='$0'>$0</a><br>", $text);
        return $text;
    }
}