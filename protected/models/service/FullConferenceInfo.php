<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 04.02.16
 * Time: 12:08
 *
 * Получение информации для комнаты конференции вынес т.к. нужно получать для обновления страницы через события и при рендеринге обычных шаблонов
 */

class FullConferenceInfo
{
    public static function get($conference_id)
    {
        $conference = ApiClient::getRoomInfo(Yii::app()->request->getParam('id'));

        $conference['speakers'] = SpeakerHelper::getSpeakerListByRoom($conference_id, $conference['room']['date']);

        if (isset($conference['room'])) {

            $conference['room']['date'] = date('d.m.Y', strtotime($conference['room']['date']));

        }

        $conference['folder'] = ApiClient::getConferenceInfo($conference_id);

        return $conference;
    }

    public static function getTitle($conference_id)
    {
        $room_id = Yii::app()->request->getParam('id');

        $room = ApiClient::getRoomInfo($room_id);
        $title = '';
        $conference['speakers'] = SpeakerHelper::getSpeakerListByRoom($conference_id, $room['room']['date']);

        foreach ($conference['speakers'] as $speaker) {

            $title .= $speaker['name'] . '.' . $speaker['theme_perfomance'] . '.';
        }

        return $title;
    }
}