/**
 * Created by artyomnorin on 13.08.15.
 */

$(function () {

    $.each(preload_banner_collection, function (i, item) {
        item.shadow = item.shadow == 1 ? true : false;
        item.active = item.active == 1 ? true : false;
    });

    var banner_collection = new App.Collections.BannerCollection(preload_banner_collection);

    new App.Views.AddBannerView();
    new App.Views.BannerCollectionView({collection: banner_collection});

    $('.btn-add-banner').click(function () {
        if($('.add-banner').length != 1){
            new App.Views.AddBannerView();
        }

    });
});