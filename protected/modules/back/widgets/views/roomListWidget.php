<?php foreach ($rooms as $room): ?>
    <?= CHtml::openTag('a', array(
        'href' => '/admin/'.Yii::app()->request->getParam('conf_id').'/room/'.$room['id'].'/',
        'class' => 'item-level_two-dropdown',
        'data-room_id' => $room['id'])) ?>
    <?php $date_formatter = new CDateFormatter('ru'); ?>
    <p class="item-date">
        <?= $date_formatter->format('d MMMM', strtotime($room['date'])); ?>
    </p>

    <p class="item-topic">
        <?= $room['name'] ?>
    </p>

    <div class="item-delete black-icon">
        <?= CHtml::image('/img/icon/delete_dark.png'); ?>
    </div>

    <div class="item-delete white-icon">
        <?= CHtml::image('/img/icon/delete_light.png'); ?>
    </div>
    <?= CHtml::closeTag('a') ?>
<?php endforeach; ?>