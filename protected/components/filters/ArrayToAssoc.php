<?php
/**
 * Фильтр преобразует линейную структуру данных
 * в струткуру ключ => значение
 * @author sp
 *
 */

class ArrayToAssoc
{
    public static function filter($rows, $field = null, $pk = 'id')
    {
        $res = array();
        if (! $rows)
            return $res;
        foreach ($rows as $row)
        {
            $attribs = ($row instanceof CActiveRecord) ? $row->getAttributes() : $row;

            if (strpos($field, '.') === false)
                $res[$attribs[$pk]] = ($field) ? $attribs[$field] : $attribs;
            else
            {
                $parts = explode('.', $field);
                $res[$attribs[$pk]] = $row->$parts[0]->$parts[1];
            }

        }
        return $res;
    }
}