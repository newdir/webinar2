<script type="text/javascript">

    initDataLayer = {
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': '<?=$direction_id?> - 0 - undefined',
        'Gtm-ee-event-action': '<?=$category?>',
        'Gtm-ee-event-label': '<?=$room_id?>',
        'userid' :  '<?=$client_id?>',
        'gtm-ee-event-non-interaction': 'True',
    };

    window.dataLayer = window.dataLayer || [];
    dataLayer.push(initDataLayer);

</script>
