<?php


class SiteController extends Controller
{
    public function actionIndex ()
    {
        if(!Yii::app()->user->getId()){
            $this->redirect('/login');
        }

        $conferences = ArrayToAssoc::filter(ApiClient::getConferenceList(UserHelper::get()), 'name', 'id');

        if (empty($conferences)) {
            throw new CHttpException(403, 'Access denied');
        }

        Yii::app()->request->redirect('/admin/' . key($conferences));
    }

    public function actionError ()
    {
        if ($error = Yii::app()->errorHandler->error){
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}