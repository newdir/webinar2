<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 01.04.16
 * Time: 9:33
 */

class RequestFilter extends CFilter
{
    public function preFilter($filter_chain)
    {
        switch (Yii::app()->request->getRequestType()) {

            case 'GET':
                $body = serialize($_GET);
                break;

            case 'DELETE':
                $body = serialize($_REQUEST);
                break;

            default:
                $body = !empty($_POST) ? serialize($_POST) : key(Yii::app()->request->getRestParams());
        }

        $controller = Yii::app()->getController();

        try {

            Yii::app()->db->createCommand()->insert("request_log", array(
                'user_id' => Yii::app()->user->id,
                'body' => $body,
                'date' => date('Y-m-d H:i:s'),
                'type' => Yii::app()->request->getRequestType(),
                'ip' => ip2long(substr(Yii::app()->request->getUserHostAddress(), 0, 16)),
                'controller' => $controller->id,
                'action' => $controller->action->id,
                'module' => isset($controller->module) ? $controller->module->id : null,
                'room_id' => Yii::app()->request->getParam('id'),
            ));
        } catch (CException $e) {}

        return true;
    }
}