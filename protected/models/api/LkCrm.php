<?php


class LkCrm
{
    const TIMELIFE_NOCACHE = 0;

    const TIMELIFE_AUTH = 2592000; //30 days

    const TIMELIFE_CONFERENCE_INFO = 180; //5 minutes

    const TIMELIFE_CONFERENCE_LIST = 180; //5 minutes

    const TIMELIFE_SPEAKER = 180;

    const TIMELIFE_SCHEDULE = 180;

    const TIMELIFE_BANNER = 60;

    const REQUEST_TIMEOUT = 20;

    const CACHEKEY_WEBSERVICE_NOT_AVAILABLE = 'ws_not_available';
    const WEBSERVICE_LOCK_TIMEOUT = 600;
    const PREFIX_PERSISTENT_CACHEKEY = 'persistent_';
    const WEBSERVICE_MASTER = 'webservice_master';
    const WEBSERVICE_REPLICATION = 'webservice_replication';

    protected static $hash = 'FNSfbY2vb2f5G9';

    protected static $methods = array(
        'getConferenceInfo' => array('timelife' => self::TIMELIFE_CONFERENCE_INFO, 'destination' => self::WEBSERVICE_REPLICATION),
        'getSpeakerList' => array('timelife' => self::TIMELIFE_SPEAKER, 'destination' => self::WEBSERVICE_REPLICATION),
        //'onCheckRights' => array('timelife' => self::TIMELIFE_CONFERENCE_INFO),
        'getConferenceList' => array('timelife' => self::TIMELIFE_CONFERENCE_LIST, 'destination' => self::WEBSERVICE_REPLICATION),
        'getSchedule' => array('timelife' => self::TIMELIFE_SCHEDULE, 'destination' => self::WEBSERVICE_REPLICATION),
        'getAdminRoom' => array('timelife' => self::TIMELIFE_SPEAKER, 'destination' => self::WEBSERVICE_REPLICATION),
        'getRoomInfo' => array('timelife' => self::TIMELIFE_SPEAKER, 'destination' => self::WEBSERVICE_REPLICATION),
        'getRoomList' => array('timelife' => self::TIMELIFE_SPEAKER, 'destination' => self::WEBSERVICE_REPLICATION),
        'getBannersList' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_REPLICATION),
        'deleteBanner' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'getRoomsToday' => array('timelife' => self::REQUEST_TIMEOUT, 'destination' => self::WEBSERVICE_REPLICATION),
        'onAuth' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'onCheckRights' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'getDaysRecordsPerConference' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_REPLICATION),
        'setRecordLinkToItemPerformance' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'onCreateBanner' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'onRegisterWebinarVisit' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'onCreateParticipations' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'saveWebinarAttendance' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'changeRoomState' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'savePerformance' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'createRoom' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'deleteRoom' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'createBanner' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_MASTER),
        'getActualRoom' => array('timelife' => self::TIMELIFE_NOCACHE, 'destination' => self::WEBSERVICE_REPLICATION),
    );

    public static function get($method, $params = array())
    {
        $timelife = 0;

        if (isset(self::$methods[$method])) {
            $timelife = self::$methods[$method]['timelife'];
        }

        if ($timelife == self::TIMELIFE_NOCACHE) {
            $data = self::_get($method, $params);
        } else {

            $room_id = Yii::app()->request->getParam('id');

            if (!is_null($room_id)) {

                $cache_key = 'room:' . $room_id . ':method:' . $method;

            } else {
                $cache_key = $method . md5(serialize($params));
            }

            $data = Yii::app()->cache->get($cache_key);

            if ($data === false) {

                $data = self::_get($method, $params);

                if ($data) {

                    try {

                        Yii::app()->cache->set($cache_key, $data, $timelife);

                    } catch(Exception $e) {

                        Yii::app()->cache->flush();
                    }
                }
            }
        }

        if ($data) {
            $data = CJSON::decode($data, true);
            $data = (isset($data['data'])) ? $data['data'] : array();

            return $data;
        }
    }

    protected static function _get($method, $params)
    {
        $ws_is_locked = Yii::app()->cache->get(self::CACHEKEY_WEBSERVICE_NOT_AVAILABLE);

        if ($ws_is_locked === true) {
            return false;
        }

        $params['method'] = $method;

        $tmp_hash = array_key_exists('hash', $params) ? $params['hash'] : null;
        $params['hash'] = self::$hash;
        $hash = md5(serialize($params));

        if(isset($tmp_hash)){
            $params['hash'] = $tmp_hash;
        }else{
            unset($params['hash']);
        }

        $attribs = array('hash' => $hash, 'params' => $params);

        $uri = self::formUriRequest($attribs);

        /*if ($method == 'getAdminRoom') {
            header('Content-Type: text/plain; charset=utf-8');
            echo urldecode($uri);
            exit;
        }*/
        $context = stream_context_create(['http' => ['timeout' => self::REQUEST_TIMEOUT]]);
        //Yii::log($uri, CLogger::LEVEL_ERROR);
        $data = @file_get_contents($uri, false, $context);

        return $data;
    }

    protected static function formUriRequest($params)
    {
        return Yii::app()->params[self::$methods[$params['params']['method']]['destination']]
                . Yii::app()->params['lk_url']
                . '?'
                . http_build_query($params);
    }

} 