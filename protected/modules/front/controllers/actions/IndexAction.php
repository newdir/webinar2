<?php

class IndexAction extends CAction
{
    public function run($conf_id, $id)
    {
        $this->getController()->redirect('https://newdirections.ru/webinars/room/' . $id . '?' . http_build_query($_GET), true, 303);
        $room = ApiClient::getAdminRoom($conf_id, $id);

        if (empty($room)) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        $performances = array_values($room['performances']);

        if (! count($performances)) {
            echo "К комнате не привязано выступления";
            exit;
        }

        $conference = ApiClient::getConferenceInfo($conf_id);

        if (
            ($conference['direction_id'] == 1 && $conference['type_id'] != 2 && strtotime($room['date']) >= strtotime('2017-11-14')) ||
            ($conference['direction_id'] != 1 && $conference['type_id'] != 2 && strtotime($room['date']) >= strtotime('2017-10-09'))
        ) {
            $this->getController()->redirect('https://newdirections.ru/webinars/room/' . $id . '?' . http_build_query($_GET), true, 303);
        }

        $no_redirect = Yii::app()->request->getQuery('no_redirect', false);

        if (!$no_redirect && count($room['performances']) === 1) {
            $webinar = current($room['performances']);

            if (isset($webinar['group_id'])) {
                $actual_room_id = ApiClient::getActualRoomByGroupId($webinar['group_id']);

                if ($id != $actual_room_id) {
                    $hash = Yii::app()->request->getQuery('hash');
                    $this->getController()->redirect('https://webinar.newdirections.ru/'. $conf_id . '/room/' . $actual_room_id . '/' . (isset($hash) ? '?hash=' . md5($conf_id . $actual_room_id . '_rmprvt') : ''), true, 303);
                }
            }
        }

        $this->getController()->layout = '/layouts/common';

        switch($room['state_id']){
            case '0':
                $this->getController()->render('room_close');
                break;
            case '1':
                $this->getController()->render($conference['render'], ['conference' => $conference, 'performances' => $performances, 'room' => $room]);
                break;
            case '2':
                $csp_id = isset($performances[0]) ? $performances[0]['id'] : null;
                VisitRegistrator::registerComing($conf_id, $csp_id);
                $this->getController()->render('room_live', ['key_hangouts' => $performances[0]['key_hangouts'], 'youtube' => $room['link_youtube']]);
                break;
            case '21':
                $csp_id = isset($performances[1]) ? $performances[1]['id'] : null;
                VisitRegistrator::registerComing($conf_id, $csp_id);
                $this->getController()->render('room_live', ['key_hangouts' => $performances[1]['key_hangouts'], 'youtube' => $room['link_youtube']]);
                break;
            case '22':
                $csp_id = isset($performances[2]) ? $performances[2]['id'] : null;
                VisitRegistrator::registerComing($conf_id, $csp_id);
                $this->getController()->render('room_live', ['key_hangouts' => $performances[2]['key_hangouts'], 'youtube' => $room['link_youtube']]);
                break;
            case '3':
                $this->getController()->render('room_pause');
                break;
            case '4':
                $this->getController()->render('room_trouble');
                break;
            case '5':
                $this->getController()->render('room_sale', ['conference' => $conference]);
                break;
            case '6':

                if (count($performances) === 1) {

                    if ($performances[0]['key_hangouts_parts']) {
                        $this->getController()->render('room_record', [
                            'performances' => $performances,
                            'record' => true,
                            'room' => $room,
                            'conference' => $conference,
                        ]);

                        return;
                    }    

                    $this->getController()->render('room_live', ['key_hangouts' => $performances[0]['key_hangouts'], 'youtube' => $room['link_youtube']]);
                    return;
                }

                $this->getController()->render('room_record', [
                    'conference' => $conference, 
                    'performances' => $performances,
                    'record' => true,
                    'room' => $room,
                ]);
                break;
            case '7':
                $this->getController()->render('room_postponed', ['date_postpone' => $room['date_postpone']]);
                break;
            case '8':
                $this->getController()->render('room_mk_is_over');
                break;
        }
    }
}