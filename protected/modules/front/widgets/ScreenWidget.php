<?php

/**
 * Виджет выводит информацию в основную область экрана
 * Class StaticScreeWidget
 */
class StaticScreeWidget extends CWidget
{
    public function run()
    {
        $path = $this->getController()->static_path;
        Yii::app()->clientScript->registerScriptFile($path . '/js/screen.js',CClientScript::POS_END);
        $this->render('screenWidget');
    }
} 