<!DOCTYPE html>
<html lang="en">
<head>

    <?php $conf_id = Yii::app()->request->getParam('conf_id')?>
    <?php $conference = ApiClient::getConferenceInfo($conf_id); ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WHD6Z8V');</script>
    <!-- End Google Tag Manager -->
    <title>Вебинар.<?= FullConferenceInfo::getTitle($conf_id) ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/css-fonts.css?v=1.1">

    <?php $conference_info = CJSON::decode($this->widget('application.modules.front.widgets.RoomJsonWidget', [], true), true); ?>

    <script type="text/javascript">
        var room_json = <?php $this->widget('application.modules.front.widgets.RoomJsonWidget'); ?>;
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript" src="/js/vendors/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="text/javascript" src="/js/respond.js"></script>

    <script type="text/javascript" src="/js/vendors/jquery/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/jquery.fancybox.css">
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
    <script type="text/javascript" src="/js/refresh.js"></script>

    <script src="https://cdn.socket.io/socket.io-1.3.5.js"></script>
    <script type="text/javascript" src="/js/vendors/jquery/jQuery.cookie.js"></script>

    <script type="text/javascript" src="/js/likely.js"></script>

    <?php if ((time() - strtotime($conference_info['speakers'][0]['date_perfomance'])) < 604800 && $this->action->id !== 'record'): ?>
        <script type="text/javascript" src="/js/io/ioinit.js?v=1.6"></script>
        <script type="text/javascript" src="/js/io/page-changes.js?v=1.0"></script>
    <?php endif; ?>

    <link rel="stylesheet" type="text/css" href="/css/correction-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/front.css?v=1.1">

    <link rel="stylesheet" type="text/css" href="/css/front_upd.css?v=1.1">
    <link rel="stylesheet" type="text/css" href="/css/likely.css">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHD6Z8V"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="container-fluid">

    <header class="row header-upd">
        <?php $this->widget('TopPanelWidget'); ?>
    </header>

    <div id="main-content" data-page="room" data-room_id="<?= ApiClient::getRoomInfo(Yii::app()->request->getParam('conf_id'))['room']['id'] ?>" class="main-content">

        <script type="text/template" id="room-live">
            <h1>Идет трансляция Online</h1>
        </script>

        <script type="text/template" id="room-trouble">
            <div class="main-content-technical">
                <div class="content-technical">
                    <img src="/img/technical-icon.png">
                    <p class="content-technical-title">Технические проблемы</p>
                    <p>Не волнуйтесь. Мы знаем о проблеме и уже исправляем её.</p>
                </div>
            </div>
        </script>

        <script type="text/template" id="room-close">
            <h1>Комната закрыта</h1>
        </script>

        <script type="text/template" id="room-pause">
            <div class="main-content-10minbreak">
                <div class="content-10minbreak">
                    <img src="/img/10minbreak-icon.png">
                    <p class="content-10minbreak-title">Перерыв на 10 минут</p>
                    <p>Спикеру нужно немного времени, чтобы отдохнуть. Отдохните и вы.</p>
                </div>
            </div>
        </script>

        <script type="text/template" id="room-sale">
            <div class="main-content-info">
                <div class="content-info">
                    <!--<div class="info-pic">-->
                    <img <%= 'src=' + room_json.folder.images %>>
                    <!--</div>-->
                    <div class="info-cost">
                        <p>Все записи конференции</p>
                        <p class="info-title"><b>«<%= room_json.folder.name %>»</b></p>
                        <div>
                            <div class="info-price item-1">
                                <img src="/img/info-newPrice.png">
                                <p><%= room_json.folder.price %></p>
                            </div>
                            <div class="info-price old-price">
                                <img src="/img/info-oldPrice.png">
                                <p><%= room_json.folder.price_old %></p>
                            </div>
                            <!--<div class="info-price item-3">
                                <p class="info-help">Скидка при покупке  до 4 февраля</p>
                            </div>-->
                        </div>
                        <div>
                            <p class="info-text">
                                <%= room_json.folder.label %>
                            </p>
                        </div>
                        <div class="info-btn">
                            <!--<button class="btn" type="button">Купить</button>-->
                            <a class="btn" target="_blank" <%= 'href=' + room_json.folder.url %> >Купить</a>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/template" id="room-greeting">
            <div class="greeting-speaker">

                <?php if (! in_array($conference['type_id'], [ ConferenceHelper::CONFERENCE_TYPE_CLOSE, ConferenceHelper::CONFERENCE_TYPE_OPEN ])): ?>

                    <div class="row conference-name">
                        <p><%= room.number_in_folder %>-й день конференции "<%= folder.name %>"</p>
                    </div>
                    <div class="row conference-date">
                        <p><%= room.date %> с <%= speakers[0].date_perfomance.substr(11, 5) %> (мск)</p>
                    </div>

                <?php endif; ?>

                <% speakers.forEach(function (speaker, i) { %>
                <div class="row row-speaker">
                    <div class="col-xs-2 conference-speaker">
                        <div class="row speaker-photo">
                            <!--<img <%= "src=" + speaker.img %> width="130" height="130">-->
                            <div class="speaker-front-photo" style="background-image: url(<%= speaker.img %>)"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 speaker-name">
                                <p><%= speaker.name %></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-10 conference-info">
                        <div class="row">
                            <div class="col-xs-12 conference-info-date">
                                <div class="conference-time">
                                    <p><%= room.date %> с <%= speaker.date_perfomance.substr(11, 5) %></p>
                                </div>
                                <div class="conference-region">
                                    <p>Время московское</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 speaker-theme">
                                <p><%= speaker.theme_perfomance %></p>
                            </div>
                        </div>
                        <div class="row conference-features">

                            <% if (speaker.description != null && speaker.description != '' && speaker.description != undefined) { %>
                            <div class="col-xs-12 features-label">
                                <p>Из мастер - класса Вы узнаете:</p>
                            </div>
                            <div class="col-xs-10 features">
                                <ul>

                                    <% speaker.description.split('\n').forEach(function (bullet) { %>
                                    <li><%= bullet.linkify() %></li>
                                    <% }); %>

                                </ul>
                            </div>
                            <% } %>

                            <% if (speaker.materials != null && speaker.materials != '' && speaker.materials != undefined) { %>
                            <div class="col-xs-12 conference-materials">
                                <div class="col-xs-12 materials-label">
                                    <p>Материалы и инструменты для мастер - класса:</p>
                                </div>
                                <div class="col-xs-10 features">
                                    <ul>

                                        <% speaker.materials.split('\n').forEach(function (material) { %>
                                        <li><%= material.linkify() %></li>
                                        <% }); %>

                                    </ul>
                                </div>
                                <!--<img src="/img/vRAdzHNd6Bw.jpg" width="640" height="433">-->
                            </div>
                            <% } %>

                        </div>
                    </div>
                </div>
                <% if(speaker.images != null && speaker.images != undefined && speaker.images != '') { %>
                <div class="row spaker-images">
                    <div class="col-xs-offset-2 col-xs-10">

                        <% speaker.images.split(',').forEach(function (image) { %>
                        <a class="fancybox-image-container" href="<%= image %>">
                            <img class="speaker-image" src="<%= image %>">
                        </a>
                        <% }); %>

                    </div>
                </div>
                <% } %>
                <% }); %>
            </div>
        </script>

        <script type="text/template" id="banner">

            <% if (output_type == 0) { %>

            <a id="<%= id %>" href="<%= url %>" target="_blank" class="main-banner" style="height: 94px!important; background: url(<%= image %>); no-repeat; background-size: 100% 100%;">

                <% } %>

                <% if (output_type == 1) { %>

                <a id="<%= id %>" href="<%= url %>" target="_blank" class="main-banner" style="min-height: 318px; background: url(<%= image %>); no-repeat; background-size: 100% 100%;">

                    <% } %>

                    <% if(shadow == true && label != ''){ %>
                    <p style="text-shadow: #000 5px 5px 8px, #000 -5px -5px 8px; color: #<%= color_label %>"><%= label %></p>
                    <% } %>

                    <% if(shadow != true && label != ''){ %>
                    <p style="color: #<%= color_label %>;"><%= label %></p>
                    <% } %>

                    <% if(button != null && button != ''){ %>
                    <span class="main-banner-button"><%= button %></span>
                    <% } %>
                </a>
        </script>

        <div class="col-sm-9 left-block">
            <?= $content; ?>
        </div>

        <div class="col-sm-3 banner-chat-wrap">
            <div class="row banners">
                <?php $this->widget('BannerWidget'); ?>
            </div>
            <div class="row">
                <div class="col-sm-12 chat-wrap">
                    <div class="row">
                        <div class="vk-comments">
                            <?php $this->widget('ChatWidget'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript" src="/js/front_main.js?v=1.0"></script>
<script type="text/javascript" src="/js/count_online.js?v=1.5"></script>

<?= ThemeHelper::includeThemeStatic(); ?>
</body>


</html>