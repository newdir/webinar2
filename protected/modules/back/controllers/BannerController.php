<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 13.08.15
 * Time: 12:12
 */

class BannerController extends Controller {

    public function filters()
    {
        return [
            'accessControl',
            ['application.modules.back.controllers.filters.ConferenceAccessControlFilter'],
            ['application.modules.back.controllers.filters.RequestFilter'],
        ];
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?')
            ),
        );
    }

    public function actionSave(){

        $param = $this->getJsonInput();

        if(($banner_id = Yii::app()->request->getParam('id_banner')) !== null){
            $param['id'] = $banner_id;
        }

        $room_id = Yii::app()->request->getParam('id');

        $room = ApiClient::getRoomInfo($room_id);
        $banner = ApiClient::createWebinarBanner(
            $banner_id,
            $room_id,
            $param['url'],
            $param['label'],
            $param['image'],
            isset($param['button']) ? $param['button'] : null,
            isset($param['color_label']) ? $param['color_label'] : '#fff',
            $param['shadow'] ? 1 : 0,
            $param['active'] ? 1 : 0,
            $param['output_type'],
            $param['speaker_id']
        );

        if($banner !== false){
            $param['id'] = $banner['id'];
            echo CJSON::encode($param);
            ApiClient::createBanner(Yii::app()->request->getParam('conf_id'), new DateTimeImmutable($room['room']['date']), $banner);
            Yii::app()->end();
        }

        return false;
    }

    public function actionDestroy()
    {
        echo ApiClient::deleteBanner(Yii::app()->getRequest()->getParam('id_banner'));
    }
}