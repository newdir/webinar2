<?php

class DefaultController extends Controller
{
    public function filters()
    {
        return [
            'accessControl',
            ['application.modules.back.controllers.filters.ConferenceAccessControlFilter'],
            ['application.modules.back.controllers.filters.RequestFilter'],
        ];
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?')
            ),
        );
    }

    public function actionIndex($conf_id, $id)
    {
        if (!Yii::app()->getRequest()->isAjaxRequest) {
            $model_room = ApiClient::getAdminRoom($conf_id, $id);

            if (!$model_room) {
                throw new CHttpException(404, 'Page not found');
            }
        }

        if(Yii::app()->getRequest()->isAjaxRequest){
            $state_id =  Yii::app()->getRequest()->getParam('state_id');
            $old_state_id =  Yii::app()->getRequest()->getParam('old_state_id');

            $user = UserHelper::get();

            RoomStateLog::log($id, $old_state_id, $state_id, $user['id']);

            if(ApiClient::changeRoomState($conf_id, $id, $state_id)){
                Yii::app()->cache->delete('room:' . $id . ':method:getAdminRoom');
                Yii::app()->cache->delete('room:' . $id . ':method:getRoomInfo');
                echo true;
                return;
            }

            echo false;
            return;
        }

        $pseudo_online = Yii::app()->redis->getClient()->get('room:' . $id . ':lie-online');

        if ($pseudo_online === false) {
            $pseudo_online = 0;
        }

        $list_speakers = SpeakerHelper::getSpeakerListByRoom($conf_id, $model_room['date']);
        $speakers = ArrayToAssoc::filter($list_speakers, 'id', 'name');
        $speakers['Не принадлежит спикеру'] = 0;

        $this->render('/layouts/common', array(
            'model_room' => $model_room,
            'room_id' => $id,
            'conf_id' => $conf_id,
            'pseudo_online' => $pseudo_online,
            'speakers' => $speakers,
        ));
    }

    public function actionDeleteRoom()
    {
        if (!Yii::app()->request->isAjaxRequest) {

            throw new CHttpException(404, 'Страница не найдена');
        }

        $data = $this->getJsonInput();

        $conference_id = Yii::app()->request->getParam('conf_id');
        $room_id = $data['room_id'];

        $result = ApiClient::deleteRoom($conference_id, $room_id);

        Yii::app()->cache->delete('room:' . $room_id . ':method:getAdminRoom');
        Yii::app()->cache->delete('room:' . $room_id . ':method:getRoomInfo');

        echo CJSON::encode($result);
    }

    public function actionError()
    {
        if (Yii::app()->request->isAjaxRequest){

            $data = $this->getJsonInput();

            file_put_contents('protected/runtime/log.txt', serialize($data) . "\n\n", FILE_APPEND);
        }
    }

    public function actionSetPseudoOnline()
    {
        if (Yii::app()->request->isAjaxRequest){

            $data = $this->getJsonInput();

            $room_id = $data['room_id'];
            $count = $data['count'];

            $result = Yii::app()->redis->getClient()->set('room:' . $room_id . ':lie-online', $count, 604800);

            echo CJSON::encode(['success' => $result]);
        }
    }
}