<?php

/**
 * Класс принимает запросы на модификацию информации о комнате
 * Class FolderController
 */
class RoomController extends Controller
{
    public function filters()
    {
        return [
            'accessControl',
            ['application.modules.back.controllers.filters.ConferenceAccessControlFilter'],
            ['application.modules.back.controllers.filters.RequestFilter'],
        ];
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?')
            ),
        );
    }

    public function actionCreate($conf_id)
    {
        if (Yii::app()->request->isPostRequest) {

            $data = Yii::app()->request->getPost('Room');

            $room_id = ApiClient::createRoom(
                $conf_id,
                $data['name'],
                $data['date'],
                $data['state_id'],
                isset($data['is_private']) ? true : false
            );

            $this->redirect('/admin/' . $conf_id . '/room/' . $room_id);
        }

        $this->render('new');
    }

    public function actionUpdate($conf_id, $id)
    {
        $data = Yii::app()->request->getPost('Room');

        if (!empty($data['date_postpone'])) {
            $data['date_postpone'] = str_replace('T', ' ', $data['date_postpone']);
        }

        ApiClient::updateRoom(
            $id,
            $conf_id,
            $data['name'],
            $data['date'],
            isset($data['is_private']) ? true : false,
            isset($data['is_publish_vk_comments']) ? true : false,
            empty($data['date_postpone']) ? null : $data['date_postpone'],
            isset($data['video_not_ready']) ? $data['video_not_ready'] : 0
        );

        Yii::app()->cache->delete('room:' . $id . ':method:getAdminRoom');
        Yii::app()->cache->delete('room:' . $id . ':method:getRoomInfo');

        $this->redirect('/admin/' . $conf_id . '/room/' . $id);
    }

    public function actionLoadSpeakers($conf_id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $param = $this->getJsonInput();

            $speakers = SpeakerHelper::getSpeakerListByRoom($conf_id, $param['date_perfomance']);

            if (empty($speakers)) {
                echo CJSON::encode(null);
            }

            echo CJSON::encode($speakers);
        }
    }

    public function actionSavePerfomance($conf_id, $id)
    {
        $perfomances = Yii::app()->request->getPost('FormPerfomance');

        $base_watch_url = Yii::app()->getBaseUrl(true) . '/' . $conf_id . '/room/' . $id .'/watch?hw=';

        ApiClient::savePerformance($conf_id, $id, $base_watch_url, $perfomances);

        Yii::app()->cache->delete('room:' . $id . ':method:getAdminRoom');
        Yii::app()->cache->delete('room:' . $id . ':method:getRoomInfo');

        $this->redirect('/admin/' . $conf_id . '/room/' . $id);
    }

    public function actionClearCache($conf_id, $id)
    {
        Yii::app()->cache->delete('room:' . $id . ':method:getAdminRoom');
        Yii::app()->cache->delete('room:' . $id . ':method:getRoomInfo');
    }
} 