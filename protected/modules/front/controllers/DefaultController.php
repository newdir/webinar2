<?php

/**
 * Контроллер обработки запросов посетителей вебинарной комнаты
 * Class DefaultController
 */
class DefaultController extends Controller
{
    public $model_class = 'RoomHandler';


    public $static_path = null;

    protected $key_hangouts = [
        '0' => 'key_hangouts',
        '1' => 'key_hangouts_2',
        '2' => 'key_hangouts_3',
    ];

    private $room_states = [
        2 => 0,
        21 => 1,
        22 => 2
    ];

    public function filters()
    {
        return [
            [
                'application.modules.front.controllers.filters.PrivateRoomAccessFilter + index',
                'url_referrer' => 'newdirections.ru',
                'cookie_expire' => '1296000',
            ]
        ];
    }

    public function actions()
    {
        return array(
            //Выводит страницу вебинарной комнаты
            'index' => array('class' => 'application.modules.front.controllers.actions.IndexAction'),
            //Выводит страницу расписания в рамках каталога
            'shedule' => array(
                'class' => 'application.components.actions.RenderWidgetAction',
                'widget_name' => 'SpeakerSheduleWidget',
            ),
            //Возвращает актуальный контент
            'content' => array(
                'class' => 'application.components.actions.ContentAction',
                'method' => 'getContent',
            ),
            //Возвращает код состояния комнаты
            'state' =>  array(
                'class' => 'application.components.actions.CommonAction',
                'method' => 'getStates',
            ),
        );
    }

    public function actionWatch($conf_id, $id)
    {
        $this->redirect('https://newdirections.ru/webinars/room/' . $id);

        $record_num = Yii::app()->request->getParam('record');

        if (is_null($record_num)) {

            throw new CHttpException(403, 'Forbidden');
        }

        $room = ApiClient::getAdminRoom($conf_id, $id);
        $performances = array_values($room['performances']);

        $hash = md5($conf_id . $id . $performances[$record_num]['key_hangouts']);

        if(Yii::app()->request->getParam('hw') == $hash){
            $this->render('watch', array('key_hangouts' => $performances[$record_num]['key_hangouts']));
            return;
        }

        throw new CHttpException(403, 'Forbidden');
    }

    public function actionSpeaker($conf_id, $id)
    {
        $this->redirect('https://newdirections.ru/webinars/room/' . $id . '?is_speaker=1&' . http_build_query($_GET), true, 303);

        $room = ApiClient::getAdminRoom($conf_id, $id);
        $conference = ApiClient::getConferenceInfo($conf_id);

        if (
            ($conference['direction_id'] == 1 && $conference['type_id'] != 2 && strtotime($room['date']) >= strtotime('2017-11-14')) ||
            ($conference['direction_id'] != 1 && $conference['type_id'] != 2 && strtotime($room['date']) >= strtotime('2017-10-09'))
        ) {
            $this->redirect('https://newdirections.ru/webinars/room/' . $id . '?is_speaker=1&' . http_build_query($_GET), true, 303);
        }

        $no_redirect = Yii::app()->request->getQuery('no_redirect', false);

        if (!$no_redirect && count($room['performances']) === 1) {
            $webinar = current($room['performances']);

            if (isset($webinar['group_id'])) {
                $actual_room_id = ApiClient::getActualRoomByGroupId($webinar['group_id']);

                if ($id != $actual_room_id) {
                    $this->redirect('https://webinar.newdirections.ru/'. $conf_id . '/room/' . $actual_room_id . '/speaker/', true, 303);
                }
            }
        }

        $performances = array_values($room['performances']);

        $this->layout = '/layouts/speaker';

        switch($room['state_id']){
            case '0':
                $this->render('room_close');
                break;
            case '1':
                $this->render($conference['render'], ['conference' => $conference, 'performances' => $performances, 'room' => $room]);
                break;
            case '2':
                $this->render('room_live', ['is_speaker_room' => true]);
                break;
            case '21':
                $this->render('room_live', ['is_speaker_room' => true]);
                break;
            case '22':
                $this->render('room_live', ['is_speaker_room' => true]);
                break;
            case '3':
                $this->render('room_pause');
                break;
            case '4':
                $this->render('room_trouble');
                break;
            case '5':
                $this->render('room_sale', ['conference' => $conference]);
                break;
            case '6':
                $this->render('room_live', ['is_speaker_room' => true]);
                break;
            case '7':
                $this->render('room_postponed', ['date_postpone' => $room['date_postpone']]);
                break;
            case '8':
                $this->render('room_mk_is_over');
                break;
        }
    }

    public function actionRecord($conf_id, $id, $record_id)
    {
        $this->layout = '/layouts/common';

        $part_id = Yii::app()->request->getParam('part');

        $room = ApiClient::getAdminRoom($conf_id, $id);

        if ($room['state_id'] != '6' || !isset($room['performances'][$record_id])) {

            $this->render('room_record_disable');
            return;
        }

        if (!is_null($part_id) && !empty($room['performances'][$record_id]['key_hangouts_parts'])) {

            $links = explode(PHP_EOL, $room['performances'][$record_id]['key_hangouts_parts']);

            if (!isset($links[$part_id])) {
                throw new CHttpException(404);
            }

            $this->render('room_live', [
                'key_hangouts' => $links[$part_id],
            ]);

        } else {

            $this->render('room_live', [
                'key_hangouts' => $room['performances'][$record_id]['key_hangouts'],
            ]);
        }
    }

    public function actionContent() {
        $resources = Yii::app()->request->getParam('resources', null);
        $result = array();

        if (in_array('banner', $resources)) {
            $result['banner'] = $this->widget('BannerWidget');
        }

        if (in_array('screen', $resources)) {
            $result['screen'] = $this->widget('StaticScreeWidget');
        }
        echo CJSON::encode($result);
    }

    public function actionState() {
        echo CJSON::encode(
            array(
                'online' => RoomHandler::getOnlineCount(),
                'states' => CHtml::listData(RoomHandler::getStates(), 'type', 'value'),
            )
        );
    }

    public function actionUnload($conf_id, $id)
    {
        return;

        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(404);
        }

        $cookie = Yii::app()->request->cookies[md5('registered_' . $conf_id)];

        if (is_null($cookie)) {
            return;
        }

        $client_id = $cookie->value;

        $room_state_id = $this->getJsonInput()['room_state_id'];

        $room = ApiClient::getAdminRoom($conf_id, $id);

        if (empty($room)) {
            throw new CHttpException(404);
        }

        $performances = array_values($room['performances']);

        $csp_id = $performances[$this->room_states[$room_state_id]]['id'];

        $tmp_visit = Yii::app()->db->createCommand()
            ->select('come_time, duration, conference_speaker_performance_id')
            ->from('tmp_visit_duration')
            ->where('client_id = :client_id AND conference_speaker_performance_id = :csp_id', [
                ':client_id' => $client_id,
                ':csp_id' => $csp_id,
            ])
            ->queryRow();

        if (empty($tmp_visit['duration']) || $tmp_visit['duration'] <= time() - $tmp_visit['come_time']) {

            try {

                Yii::app()->db->createCommand()
                    ->update('tmp_visit_duration',
                        ['duration' => time() - $tmp_visit['come_time']],
                        'client_id = :client_id AND conference_speaker_performance_id = :csp_id',
                        [':client_id' => $client_id, ':csp_id' => $csp_id]
                    );
                return;

            } catch (CException $e) {}
        }
    }

    public function actionRegisterComing($conf_id, $id)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(404);
        }

        $room_state_id = $this->getJsonInput()['room_state_id'];

        $room = ApiClient::getAdminRoom($conf_id, $id);

        if (empty($room)) {
            throw new CHttpException(404);
        }

        $performances = array_values($room['performances']);

        $csp_id = $performances[$this->room_states[$room_state_id]]['id'];

        VisitRegistrator::registerComing($conf_id, $csp_id);
    }

    public function actionCountOnline()
    {
        $room_id = Yii::app()->request->getParam('room_id');

        $count_online = 0;

        $pseudo_online = Yii::app()->redis->getClient()->get('room:' . $room_id . ':lie-online');

        if ($pseudo_online !== false) {
            $count_online += $pseudo_online;
        }

        $room_ips = new ARedisSet('room:' . $room_id . ':ips');

        $count_online += $room_ips->count();

        echo CJSON::encode(['count' => $count_online, 'success' => true]);
    }
}