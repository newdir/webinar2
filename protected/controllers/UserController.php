<?php

class UserController extends Controller
{
    public $layout='//layouts/login';

    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
        );
    }

    public function actionLogin(){

        $errors = array();

        $form_model = new LoginForm();

        if(!Yii::app()->user->getId() && !Yii::app()->request->isPostRequest){
            $this->render('login', array('model' => $form_model));
            return;
        }

        if(($data = Yii::app()->getRequest()->getPost('LoginForm')) !== null){

            $form_model->username = strtolower(trim($data['username']));
            $form_model->password = trim($data['password']);
            $form_model->remember_me = boolval($data['remember_me']);

            if(!$form_model->validate()){
                $this->render('login', array('model' => $form_model));
                return;
            }


            if(!$form_model->login()){
                $errors[] = 'Неверный логин или пароль';
                $this->render('login', array('model' => $form_model, 'errors' => $errors));
                return;
            }
        }

        $conferences = ArrayToAssoc::filter(ApiClient::getConferenceList(UserHelper::get()), 'name', 'id');

        if (empty($conferences)) {
            throw new CHttpException(403, 'Access denied');
        }

        Yii::app()->request->redirect('/admin/' . key($conferences));
    }

    public function actionLogout(){
        Yii::app()->user->logout(true);

        $this->redirect('/login');
    }
}