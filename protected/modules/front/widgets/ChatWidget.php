<?php

/**
 * Виджет выводит чат комнаты
 * Class BannerWidget
 */
class ChatWidget extends CWidget{

    public function run()
    {
        Yii::app()->clientScript->registerScriptFile('//vk.com/js/api/openapi.js?116');

        $conference_id = Yii::app()->request->getParam('conf_id');
        $conference_info = ApiClient::getConferenceInfo($conference_id);

        $link = '/' . Yii::app()->request->getParam('conf_id') . '/room/' . Yii::app()->request->getParam('id') . '/';

        $room_id = Yii::app()->request->getParam('id');
        $room = ApiClient::getRoomInfo($room_id);

        if($room_id == '44' || $room_id == '28' || $room_id == '19'){
            $conference_info['key_vk_api'] = 5267834;
        }

        $this->render('chatWidget', array(
            'type_chat' => $room['room']['type_chat'],
            'vk_id' => $conference_info['key_vk_api'],
            'vk_page' => '',
            'link' => $link,
            'vk_comment_publish' => intval($room['room']['is_publish_vk_comments']),
        ));
    }

}