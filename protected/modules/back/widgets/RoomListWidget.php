<?php

/**
 * Виджет выводит список комнат
 * Class RoomListWidget
 */
class RoomListWidget extends CWidget {

    public function run()
    {
        $conference_id = Yii::app()->request->getParam('conf_id');

        $rooms = ApiClient::getRoomList($conference_id);

        $this->render('roomListWidget', array('rooms' => $rooms));
    }
}