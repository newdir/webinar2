<?php foreach ($banners as $banner): ?>

    <?php $sign = '?'; ?>

    <?php if (strpos($banner['url'], '?') !== false): ?>
        <?php $sign = '&' ?>
    <?php endif; ?>

    <?php if ($banner['output_type'] == 0): ?>

        <a id="<?= $banner['id']; ?>" href="<?= $banner['url'] ?>" target="_blank" class="main-banner"
        style="height: 94px !important; background: url(<?= $banner['image'] ?>) no-repeat; background-size: 100% 100%;">

    <?php elseif ($banner['output_type'] == 1): ?>

        <a id="<?= $banner['id']; ?>" href="<?= $banner['url'] ?>" target="_blank" class="main-banner"
        style="min-height: 318px; background: url(<?= $banner['image'] ?>) no-repeat; background-size: 100% 100%;">

    <?php endif; ?>

    <?php if($banner['shadow'] == 1 && !empty($banner['label'])): ?>
        <p style="text-shadow: #000 5px 5px 8px, #000 -5px -5px 8px; <?= 'color: #'.$banner['color_label'] ?>"><?= $banner['label'] ?></p>
    <?php endif; ?>

    <?php if($banner['shadow'] != 1 && !empty($banner['label'])):  ?>
        <p style="<?= 'color: #'.$banner['color_label'] ?>" ><?= $banner['label'] ?></p>
    <?php endif; ?>

    <?php if(!empty($banner['button'])): ?>
        <span class="main-banner-button"><?= $banner['button'] ?></span>
    <?php endif; ?>
    </a>
<?php endforeach; ?>