<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 14.12.15
 * Time: 12:33
 */

class ApiClient
{
    public static function getRoomList($conference_id)
    {
        return LkCrm::get('getRoomList', [
            'conference_id' => (string) $conference_id,
        ]);
    }

    public static function getRoomInfo($room_id)
    {
        return LkCrm::get('getRoomInfo', [
            'room_id' => (string) $room_id,
        ]);
    }

    public static function onAuth($email)
    {
        $auth_hash = md5('smd5' . $email . 'auth_sp');
        return LkCrm::get('onAuth', array(
            'email' => $email,
            'hash' => $auth_hash,
        ));
    }


    public static function onCheckRights($user, $conference_id)
    {
        $hash = UserHelper::getHash($user);
        return LkCrm::get('onCheckRights', [
            'hash' => $hash,
            'cabinet_id' => $conference_id,
            'user_id' => (string)$user['id']
        ]);
    }


    public static function getConferenceList($user)
    {
        $hash = UserHelper::getHash($user);
        return LkCrm::get('getConferenceList', [
            'hash' => $hash,
            'user_id' => (string)$user['id'],
        ]);
    }

    public static function getDaysRecordsPerConference(array $conference_ids)
    {
        return LkCrm::get('getDaysRecordsPerConference', [
            'conference_ids' => $conference_ids,
        ]);
    }

    public static function getSpeakerList($conference_id)
    {
        $hash = UserHelper::getHash();
        return LkCrm::get('getSpeakerList', [
            'conference_id' => $conference_id,
            'hash' => $hash,
        ]);
    }

    public static function getConferenceInfo($conference_id)
    {
        $hash = UserHelper::getHash();
        return LkCrm::get('getConferenceInfo', [
            'conference_id' => $conference_id,
            'hash' => $hash,
        ]);
    }

    public static function getSchedule($conference_id)
    {
        $hash = UserHelper::getHash();
        return LkCrm::get('getSchedule', [
            'id' => $conference_id,
            'hash' => $hash,
        ]);
    }

    /*public static function setRecordLinkToItemPerformance($conference_id, $date_performance, array $record_links, array $keys_hangouts)
    {
        $record_links = array_map(function ($item) {
            return strval($item);
        }, $record_links);

        $keys_hangouts = array_map(function ($item) {
            return strval($item);
        }, $keys_hangouts);

        $hash = UserHelper::getHash();
        return LkCrm::get('setRecordLinkToItemPerformance', [
            'id' => strval($conference_id),
            'date_performance' => $date_performance,
            'record_links' => $record_links,
            'keys_hangouts' => $keys_hangouts,
            'hash' => $hash,
        ]);
    }*/

    public static function createBanner($conference_id, DateTimeInterface $room_date, array $banner)
    {
        $hash = UserHelper::getHash();
        return LkCrm::get('onCreateBanner', [
            'conference_id' => $conference_id,
            'speaker_id' => $banner['speaker_id'],
            'img' => $banner['image'],
            'url' => $banner['url'],
            'hash' => $hash,
            'id' => $banner['id'],
            'date_performance' => $room_date->format('Y-m-d'),
        ]);
    }

    public static function registerWebinarVisit($conference_id, $email)
    {
        $hash = UserHelper::getHash();
        return LkCrm::get('onRegisterWebinarVisit', [
            'hash' => $hash,
            'email' => strtolower(trim($email)),
            'conference_id' => strval($conference_id),
        ]);
    }

    public static function createParticipation(array $participations)
    {
        return LkCrm::get('onCreateParticipations', [
            'participations' => $participations,
        ]);
    }

    public static function saveWebinarAttendance(array $rooms)
    {
        return LkCrm::get('saveWebinarAttendance', [
            'rooms' => $rooms,
        ]);
    }

    public static function getAdminRoom($conference_id, $room_id)
    {
        return LkCrm::get('getAdminRoom', [
            'room_id' => (string) $room_id,
            'conference_id' => (string) $conference_id,
        ]);
    }

    public static function changeRoomState($conference_id, $room_id, $state_id)
    {
        return LkCrm::get('changeRoomState', [
            'room_id' => (string) $room_id,
            'conference_id' => (string) $conference_id,
            'state_id' => (string) $state_id,
        ]);
    }

    public static function savePerformance($conference_id, $room_id, $base_watch_url, array $performances)
    {
        return LkCrm::get('savePerformance', [
            'room_id' => (string) $room_id,
            'performances' => $performances,
            'conference_id' => (string) $conference_id,
            'base_watch_url' => $base_watch_url
        ]);
    }

    public static function createRoom($conference_id, $name, $date, $state_id, $is_private)
    {
        return LkCrm::get('createRoom', [
            'conference_id' => (string) $conference_id,
            'name' => $name,
            'date' => (string) $date,
            'state_id' => (string) $state_id,
            'is_private' => (string) $is_private
        ]);
    }

    public static function updateRoom($room_id, $conference_id, $name, $date, $is_private, $is_publish_vk_comments, $date_postpone, $video_not_ready)
    {
        $params = [
            'room_id' => (string) $room_id,
            'conference_id' => (string) $conference_id,
            'name' => $name,
            'date' => $date,
            'is_private' => (string) $is_private,
            'is_publish_vk_comments' => (string) $is_publish_vk_comments,
            'video_not_ready' => (string)$video_not_ready
        ];

        if (!empty($date_postpone)) {
            $params['date_postpone'] = $date_postpone;
        }

        return LkCrm::get('createRoom', $params);
    }

    public static function deleteRoom($conference_id, $room_id)
    {
        return LkCrm::get('deleteRoom', [
            'room_id' => (string) $room_id,
            'conference_id' => (string) $conference_id,
        ]);
    }

    public static function createWebinarBanner($id, $room_id, $url, $label, $image, $button, $color_label, $shadow, $active, $output_type, $speaker_id)
    {
        return LkCrm::get('createBanner', [
            'id' => (string) $id,
            'room_id' => (string) $room_id,
            'url' => (string) $url,
            'label' => (string) $label,
            'image' => (string) $image,
            'button' => (string) $button,
            'shadow' => (string) $shadow,
            'active' => (string) $active,
            'output_type' => (string) $output_type,
            'speaker_id' => (string) $speaker_id,
            'color_label' => (string) $color_label,
        ]);
    }

    public static function deleteBanner($banner_id)
    {
        return LkCrm::get('deleteBanner', ['id' => (string) $banner_id]);
    }

    public static function getBannersList($room_id)
    {
        return LkCrm::get('getBannersList', [
            'room_id' => (string) $room_id,
        ]);
    }

    public static function getRoomsToday()
    {
        return LkCrm::get('getRoomsToday', []);
    }

    public static function getActualRoomByGroupId($group_id)
    {
        return LkCrm::get('getActualRoom', [
            'group_id' => (string)$group_id
        ]);
    }
}