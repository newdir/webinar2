<?php if (isset($is_speaker_room) && $is_speaker_room): ?>
    <h1>Идет трансляция Online</h1>
<?php else: ?>
    <?php if (!empty($youtube)): ?>

        <div style="background-color: #000000; z-index: 999; color: white; position: absolute">Если у вас не воспроизводится трансляция перейдите по
            <a style="font-size: 18px;font-weight: bold; color: #ff0000" target="_blank" href="<?= $youtube ?>">ссылке на трансляцию в youtube</a></div>
    <?php endif; ?>
    <?= preg_replace("/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?/", "$0?modestbranding=1&autoplay=1&rel=0&showinfo=0", $key_hangouts); ?>
<?php endif; ?>