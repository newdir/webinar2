<?php

/**
 * Хелпер для удобства подключения статики темы
 * Class ThemeHelper
 */
class ThemeHelper
{
    const THEMES_DIR = 'themes';

    public static function includeThemeStatic()
    {
        $conf_id = Yii::app()->request->getParam('conf_id');
        $conference_info = ApiClient::getConferenceInfo($conf_id);

        if (empty($conference_info['theme'])) {
            return;
        }

        $path_assets = Yii::app()->assetManager->publish(self::THEMES_DIR.DIRECTORY_SEPARATOR.$conference_info['theme'], false, -1, true);

        $include_string = CHtml::cssFile($path_assets.'/css/theme.css')."\n";

        $include_string .= CHtml::scriptFile($path_assets.'/js/theme.js')."\n";

        return $include_string;
    }
}