<?php if($type_chat == 0): ?>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk_id?>, onlyWidgets: true});
    </script>

    <div id="vk_comments"></div>
    <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 5, mini: 1, height:550 ,attach: "*", autoPublish: <?= $vk_comment_publish ?>, pageUrl: "<?= 'http://webinar.newdirections.ru' . $link ?>"}, "<?= $link ?>");
    </script>

<?php else: ?>

    <div class="b-comments">
        <div class="chat_messages_block">
            <ul class="chat_messages">

            </ul>
        </div>
        <form class="send_form" action="">
            <input id="message_box" autocomplete="off">
            <button>Отправить</button>
        </form>
    </div>
    <div class="logout">
        <a href="#" class="logout">Выйти</a>
    </div>
    <div class="reg_wrap">
        <a href="#">Зарегистрируйтесь</a> или <a href="#">авторизируйтесь</a> <br> при помощи нашего сайта: <br>

        <div class="b-registration">
            <form class="registration" action="">
                <input type="email" class="email" autocomplete="on" placeholder="email"><br>
                <input type="text" class="nick" placeholder="Ник"><br>
                <input type="password" class="pass" placeholder="Пароль"><br>
                <button type="submit">Зарегистрироваться</button>
            </form>
        </div>

        <div class="b-registration">
            <form class="authentication" action="">
                <input type="email" class="email" autocomplete="on" placeholder="email"><br>
                <input type="password" class="pass" placeholder="Пароль"><br>
                <button type="submit">Войти</button>
            </form>
        </div>
        <br>
        <div class="b-forget">
            <form class="forget" action="">
                <p>Забыли пароль ?</p>
                <input type="email" class="email" autocomplete="on" placeholder="email"><br>
                <button type="submit">Отправить напоминание</button>
            </form>
        </div>
    </div>

<?php endif; ?>