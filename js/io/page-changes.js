function setAutoplay(iframe){
    var $iframe = $(iframe);
    var src = $iframe.attr('src');
    src += '?fs=0&modestbranding=1&autoplay=1&rel=0&showinfo=0';

    return $iframe.attr('src', src).prop('outerHTML');
}

var changePage = function (data){
    var main_content_block = $('.left-block');
    var banners = $('.banners');

    var template;

    function redrawRoom () {
        var header_height = $('header').height();
        var body_height = $(window).height();

        var delta_height = body_height - header_height;

        $('.left-block').css('min-height', delta_height + 'px');
        $('.left-block > iframe').css('height', delta_height + 'px');
    }

    var redrawBanner = function (){
        var banner = $('.banners').find('.main-banner');
        var banner_wight = banner.width();
        var banner_height = (banner_wight * 125) / 486;

        banner.css('height', banner_height + 'px');
    };

    switch (data.status){
        case '0':
            updateDataLayer('12 - Вебинар1 - просмотр');
            registerUnload();
            setRoomState(data.status);
            template = _.template($('#room-close').html());
            main_content_block.html(template);
            main_content_block.append(addYoutubeOver());
            break;

        case '1':
            updateDataLayer('12 - Вебинар1 - просмотр');
            registerUnload();
            setRoomState(data.status);
            main_content_block.html(room_json.greeting);
            main_content_block.append(addYoutubeOver());
            redrawRoom();
            redrawBanner();
            break;

        case '2':
            updateDataLayer('13 - Вебинар2 - просмотр');
            registerUnload();
            //registerComing(data.status);
            setRoomState(data.status);
            template = _.template($('#room-live').html());
            main_content_block.html(template({key_hangouts: data.key_hangouts}));
            main_content_block.append(addYoutubeOver());
            redrawRoom();
            redrawBanner();
            break;

        case '21':
            updateDataLayer('13 - Вебинар2 - просмотр');
            registerUnload();
            //registerComing(data.status);
            setRoomState(data.status);
            template = _.template($('#room-live').html());
            main_content_block.html(template({key_hangouts: data.key_hangouts}));
            main_content_block.append(addYoutubeOver());
            redrawRoom();
            redrawBanner();
            break;

        case '22':
            updateDataLayer('13 - Вебинар2 - просмотр');
            registerUnload();
            //registerComing(data.status);
            setRoomState(data.status);
            template = _.template($('#room-live').html());
            main_content_block.html(template({key_hangouts: data.key_hangouts}));
            main_content_block.append(addYoutubeOver());
            redrawRoom();
            redrawBanner();
            break;

        case '3':
            updateDataLayer('13 - Вебинар2 - просмотр');
            registerUnload();
            setRoomState(data.status);
            template = _.template($('#room-pause').html());
            main_content_block.html(template);
            main_content_block.append(addYoutubeOver());
            break;

        case '4':
            updateDataLayer('13 - Вебинар2 - просмотр');
            registerUnload();
            setRoomState(data.status);
            template = _.template($('#room-trouble').html());
            main_content_block.html(template);
            main_content_block.append(addYoutubeOver());
            break;

        case '5':
            updateDataLayer('14 - Вебинар3 - просмотр');
            registerUnload();
            setRoomState(data.status);
            template = _.template($('#room-sale').html());
            main_content_block.html(template);
            main_content_block.append(addYoutubeOver());
            break;

        case '6':
            updateDataLayer('14 - Вебинар3 - просмотр');
            if (room_json.room.key_hangouts_2 === '') {

                template = _.template($('#room-live').html());
                main_content_block.html(template({key_hangouts: data.key_hangouts}));
                main_content_block.append(addYoutubeOver());
                break;
            }

            template = _.template($('#room-record').html());

            main_content_block.html(template({room: room_json.room, speakers: room_json.speakers, folder: room_json.folder, record: true}));
            main_content_block.append(addYoutubeOver());
            redrawRoom();
            redrawBanner();
            break;

        case '8':
            template = _.template($('#mk-is-over').html());
            main_content_block.html(template);
            main_content_block.append(addYoutubeOver());
            break;

        case '7':
        case '99':
            var time = Math.floor(Math.random() * 20);

            setTimeout(function () {
                location.reload();
            }, time*1000);
            break;

        case 98:
            console.log(data);
            if(data.banner.active == true){
                if($('.main-banner#' + data.banner.id).length == 0)
                {
                    template = _.template($('#banner').html());

                    if (data.banner.url.indexOf('?') !== -1) {
                        data.sign = '&';
                    } else {
                        data.sign = '?';
                    }

                    banners.append(template(data));
                }
            }

            if(data.banner.active == false){
                var banner = $('.main-banner#' + data.banner.id);
                var banner_btn = $('.main-banner#' + data.banner.id).next('.main-banner-button');

                banner.remove();
                banner_btn.remove();
            }
            break;

        default:
            main_content_block.html('<div class="room-content">' + data.html + '</div>');
            main_content_block.append(addYoutubeOver());
            break;
    }

    function updateDataLayer(category) {
        var object = jQuery.extend(true, {}, initDataLayer);

        object.Category = category;

        dataLayer.push(object);
    }
    
    function addYoutubeOver()
    {
        return '<div class="youtube-over"></div>'
    }

    function registerComing(status)
    {
        $.ajax({
            url: 'coming/',
            method: 'POST',
            data: JSON.stringify({room_state_id: status}),
            dataType: 'json'
        });
    }

    function setRoomState(status)
    {
        $('body').data('room_state_id', status);
    }

    function registerUnload()
    {
        var room_state_id = $('body').data('room_state_id');

        if (room_state_id == 2 || room_state_id == 21 || room_state_id == 22) {

            $.ajax({
                url: 'unload/',
                method: 'POST',
                dataType: 'json',
                data: JSON.stringify({room_state_id: room_state_id}),
                async: false
            });
        }
    }
};