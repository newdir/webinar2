<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 17.02.16
 * Time: 11:59
 */

class SpeakerHelper {

    public static function getSpeakerListByRoom($conference_id, $room_date)
    {
        $speakers = [];
        $all_speakers = ApiClient::getSpeakerList($conference_id);

        if (!isset($room_date)) {
            return $speakers;
        }

        $room_date = new DateTimeImmutable($room_date);

        if (empty($all_speakers)) {
            return $speakers;
        }

        foreach ($all_speakers as $speaker) {

            if (empty($speaker['date_perfomance'])) {
                continue;
            }

            $speaker_date = new DateTimeImmutable($speaker['date_perfomance']);

            $date_diff = $room_date->diff($speaker_date);

            if ($date_diff->days == 0 && $date_diff->invert == 0 && $speaker['active']) {
                $speakers[] = $speaker;
            }
        }

       return $speakers;
    }
}