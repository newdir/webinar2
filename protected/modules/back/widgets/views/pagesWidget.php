<?php
/**
 * @var string $base_url
 * @var string $records_hash
 * @var string $speaker_link
 * @var string $client_link
 */
?>
<div class="pull-right">
    <p><?= CHtml::link('Страница для посетителей', $client_link, array('target' => '_blank')) ?></p>
    <p><?= CHtml::link('для менеджера', '#', array('target' => '_blank')) ?></p>
    <p><?= CHtml::link('для спикера', $speaker_link, array('target' => '_blank')) ?></p>
</div>