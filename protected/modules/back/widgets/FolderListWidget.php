<?php

/**
 * Виджет выводит список каталогов
 * Class FolderListWidget
 */
class FolderListWidget extends CWidget
{
    public function run()
    {
        $conferences = ArrayToAssoc::filter(ApiClient::getConferenceList(UserHelper::get()), 'name', 'id');

        $this->render('folderListWidget', array('conferences' => $conferences,
                                                'current_conf' => Yii::app()->request->getParam('conf_id')));
    }
}