<?php

/**
 * This is the model class for table "banner".
 *
 * The followings are the available columns in table 'banner':
 * @property integer $id
 * @property integer $room_id
 * @property string $src
 * @property string $url
 * @property integer $state_id
 * @property string $html
 * @property string $about
 * @property integer $weight
 * @property string $label
 * @property integer $active
 * @property string $image
 * @property string $button
 * @property string $color_label
 * @property integer $shadow
 * @property integer $output_type
 * @property integer $speaker_id
 */
class Banner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner';
	}

    public function behaviors(){

        return array(
            'changeLogBehavior' => array(
                'class' => 'application.modules.back.models.behaviours.ChangeLogBehavior'
            )
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('room_id, state_id, weight, active, shadow, output_type, speaker_id', 'numerical', 'integerOnly'=>true),
			array('button', 'length', 'max'=>25),
			array('color_label', 'length', 'max'=>4),
			array('src, url, html, about, label, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, room_id, src, url, state_id, html, about, weight, label, active, image, button, color_label, shadow, output_type, speaker_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'room_id' => 'Room',
			'src' => 'Src',
			'url' => 'Url',
			'state_id' => 'State',
			'html' => 'Html',
			'about' => 'About',
			'weight' => 'Weight',
			'label' => 'Label',
			'active' => 'Active',
			'image' => 'Image',
			'button' => 'Button',
			'color_label' => 'Color Label',
			'shadow' => 'Shadow',
			'output_type' => 'Output Type',
			'speaker_id' => 'Speaker',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('room_id',$this->room_id);
		$criteria->compare('src',$this->src,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('state_id',$this->state_id);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('button',$this->button,true);
		$criteria->compare('color_label',$this->color_label,true);
		$criteria->compare('shadow',$this->shadow);
		$criteria->compare('output_type',$this->output_type);
		$criteria->compare('speaker_id',$this->speaker_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
