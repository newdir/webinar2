<?php

class FrontModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'front.models.*',
			'front.components.*',
		));
	}
}
