<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 09.09.16
 * Time: 9:14
 */

class ParticipationCommand extends CConsoleCommand
{
    public function run()
    {
        $visits = Yii::app()->db->createCommand()
            ->select('id, client_id, conference_speaker_performance_id, come_time, duration')
            ->from('tmp_visit_duration')
            ->where('sended = false')
            ->queryAll();

        ApiClient::createParticipation($visits);

        if (!empty($visits)) {

            foreach ($visits as $visit) {

                try {

                    Yii::app()->db->createCommand()
                        ->update(
                            'tmp_visit_duration',
                            ['sended' => true],
                            'id = :id',
                            [':id' => $visit['id']]
                        );

                } catch (CException $e) {}
            }
        }
    }
}