<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 24.08.15
 * Time: 13:37
 *
 * Виджет для передачи json на фронт, при переходе пользователя на страницу
 */

class RoomJsonWidget extends CWidget {

    public function run(){

        $conf_id = Yii::app()->request->getParam('conf_id');

        $conference_info = FullConferenceInfo::get($conf_id);

        $room_greeting = new RoomGreetingWidget();
        $room_greeting->conference = $conference_info['folder'];
        $room_greeting->room = $conference_info['room'];
        $room_greeting->speakers = $conference_info['speakers'];
        $room_greeting->render_template = $conference_info['folder']['render'];

        ob_start();

        $room_greeting->run();

        $conference_info['greeting'] = ob_get_contents();

        ob_end_clean();

        echo CJSON::encode($conference_info);
    }
}