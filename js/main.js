$(function()
{
    $('.btn-clear-cache').click(function () {

        $.ajax({
            url: 'clearcache/',
            method: 'POST',
            dataType: 'json',
            statusCode: {
                200: function () {
                    alert('Кэш успешно очищен. После закрытия сообщения страница будет перезагружена');
                    location.reload();
                },
                500: function () {
                    alert('Внутренняя ошибка сервиса. Обратитесь в поддержку');
                }
            }
        })
    });

    $('.add_perf_parts').click(function (event) {
        event.preventDefault();

        var next_form_group = $(this).parents('.col-xs-12').next();

        if (next_form_group.is(':visible')) {

            next_form_group.hide();
            addBottomMarginToContentForm(-150);

        } else {

            next_form_group.show();
            addBottomMarginToContentForm(150);
        }
    });

    function addBottomMarginToContentForm(value)
    {
        var content_room = $('.content-room');

        content_room.css('margin-bottom', parseInt(content_room.css('margin-bottom')) + value);
    }

    $('#add-speaker').bind({
        click: function()
        {
            const COUNT_ADD_SPEAKER_ON_PAGE = 2;

            var $thisParent = $(this).parent();
            //$thisParent.siblings(".content-edit").find(".saveEdit").trigger('click');

            if($('.add-speaker').length == COUNT_ADD_SPEAKER_ON_PAGE){
                return;
            }

            var div_add_speaker = $('.add-speaker').clone(true).css('display', 'block');
            $thisParent.before(div_add_speaker);
        }
    });

    $('.room-date-select').change(function () {
        $('.load-speaker').remove();

        var that = $(this);
        var template = _.template($('#load-speaker-list').html());
        var speakers_input = $('.list-speakers');
        var speaker_obj = {};

        speakers_input.val(undefined);

        $.ajax({
            url: document.location.href + 'loadspeakers/',
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify({date_perfomance: that.val()}),
            success: function (data) {
                if (data !== null) {
                    $('.load-speaker').remove();
                    $.each(data, function (i, value) {
                        speaker_obj[value.id] = value.cs_id;
                    });
                    speakers_input.val(JSON.stringify(speaker_obj));
                    $('.btn-with-load-speaker').parents('.form-group').before(template({speakers: data}));
                }
            }
        });
    });

    $('.change-state-room').bind({
        click:function () {
            var button = $(this);
            var button_active = $('.change-state-room.active');

            $.ajax({
                data: {'state_id': button.attr('value'), old_state_id: button_active.attr('value')},
                success: function (data) {
                    if(data){
                        $('.change-state-room').removeClass('active');
                        button.addClass('active');
                        return;
                    }
                    alert('Ошибка смены состояния комнаты! Обратитесь в поддержку.');
                }
            });
        }
    });

    $('.pseudo_user_online_range').on('input change', function () {
        $('.pseudo_user_online_label > p').text($(this).val());
    }).trigger('input');

    $('#title-folders').change(function (event) {
        window.location = window.location.origin + '/admin/' + $(event.currentTarget).val();
    });

    $('.item-level_two-dropdown').mouseenter(function (event) {
        $(this).find('.white-icon').css('display', 'block');
    }).mouseleave(function (event) {
        $(this).find('.white-icon').css('display', 'none');
    });

    $('.item-delete.white-icon').click(function (event) {

        event.preventDefault();

        var result = confirm('Вы уверены, что хотите удалить комнату?');
        var room_item = $(this).parents('a.item-level_two-dropdown');
        if (result === false) {
            return;
        }

        $.ajax({
            method: 'POST',
            url: 'deleteroom/',
            data: JSON.stringify({room_id: room_item.data('room_id')}),
            dataType: 'json',
            success: function (data) {
                if (data.success === true) {
                    room_item.remove();
                    return;
                }

                if (data.hasOwnProperty('message')) {

                    alert(data.message);
                    return;
                }

                alert('Ошибка удаления комнаты. Обратитесь в поддержку!');
            }
        });
    });

    $('#title-folders').select2();
});