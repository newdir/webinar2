<div class="greeting-speaker">
    <div class="content_block">
        <div class="content_header">
            <div class="content_header_image">
                <img src="/img/logo_greeting.png" width="170" height="69">
            </div>

            <?php if ($conference['direction_id'] == '1'): ?>

                <div class="content_header_image">
                    <img src="/img/MDU.png" width="84" height="69">
                </div>

            <?php endif; ?>

            <div class="content_header_info">
                <p class="content_header_info_label"><?= $conference['name']; ?></p>
            </div>
        </div>

        <?php if ($conference['type_id'] !== 2): ?>

            <div class="content_days">

                <?php if ($conference['direction_id'] != 14): ?>

                    <div class="content_days_left content_days_class">

                        <?php if ($room['number_in_folder'] === '1'): ?>

                            <p class="content_days_label"></p>
                        <?php else: ?>

                            <p class="content_days_label">
                                <a target="_blank" class="content_days_link" href="<?= '/' . $conference['id'] . '/room/' . $room['prev']['id'] ?>">
                                    День <?= $room['number_in_folder'] - 1; ?>
                                </a>
                            </p>
                            <p class="content_days_info">
                                <a class="content_days_link">
                                    <?=  Yii::app()->dateFormatter->format("dd MMMM",  strtotime($room['prev']['date'])) ?>
                                </a>
                            </p>
                        <?php endif; ?>
                    </div>


                    <div class="content_days_current content_days_class">
                        <p class="content_days_label">
                            <a class="content_days_link" href="http://newdirections.ru">
                                День <?= $room['number_in_folder']; ?>
                            </a>
                        </p>
                        <p class="content_days_info">
                            <a class="content_days_link">
                                <?=  Yii::app()->dateFormatter->format("dd MMMM",  strtotime($room['date'])) ?>
                            </a>
                        </p>
                        <div class="content_days_circle"></div>
                    </div>


                    <div class="content_days_right content_days_class">

                        <?php if ($room['next'] === false): ?>

                            <p class="content_days_label"></p>

                        <?php else: ?>

                            <p class="content_days_label">
                                <a target="_blank" class="content_days_link" href="<?= '/' . $conference['id'] . '/room/' . $room['next']['id'] ?>">
                                    День <?= $room['number_in_folder'] + 1; ?>
                                </a>
                            </p>
                            <p class="content_days_info">
                                <a class="content_days_link">
                                    <?=  Yii::app()->dateFormatter->format("dd MMMM",  strtotime($room['next']['date'])) ?>
                                </a>
                            </p>
                        <?php endif; ?>
                    </div>

                <?php endif; ?>

            </div>

        <?php endif; ?>

        <div class="content_speakers">
            <?php if (!empty($performances)): ?>
                <?php foreach ($performances as $speaker): ?>

                    <div class="content_speaker_row">
                        <div class="content_speaker_image content_speaker_class"><img src="<?= $speaker['speaker_img'] ?>"></div>
                        <div class="content_speaker_info content_speaker_class">
                            <p class="content_speaker_name"><?= $speaker['speaker_name']; ?></p>
                            <div class="content_speaker_time content_speaker_class"><?= substr($speaker['date'], 11, 5) ?></div>
                            <p class="content_speaker_theme"><?= $speaker['theme'] ?></p>
                            <?php if(!empty($speaker['description'])): ?>
                                <?= BulletHtml::get($speaker['description']); ?>
                            <?php endif; ?>

                            <?php if (!empty($speaker['video'])): ?>

                                <div class="col-xs-10 features">

                                    <?= preg_replace(["/width=\"\d+\"/", "/height=\"\d+\"/"], ['width="480"', 'height="360"'], $speaker['video']) ?>

                                </div>

                            <?php endif; ?>

                            <!--<p class="content_speaker_help_info">На вебинаре мы сошьем забавного зверя прямо в рамках урока на конференции, научимся работать с
                                тканью, привильно набивать детали, раскрашивать и тонировать изделие.</p>-->

                            <?php if (!empty($speaker['materials'])): ?>

                                <span class="content_speaker_link"><b>Материалы для вебинара</b></span>
                                <p>(нажмите, чтобы увидеть)</p>
                                <div class="content_speaker_materials" style="display: none;">

                                    <?php /*foreach (explode("\n", $speaker['materials']) as $material): */?>

                                    <?= BulletHtml::get($speaker['materials']) ?>

                                    <?php /*endforeach; */?>

                                </div>

                            <?php endif; ?>
                        </div>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</div>