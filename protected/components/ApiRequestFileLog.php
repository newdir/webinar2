<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 06.06.16
 * Time: 15:38
 */

class ApiRequestFileLog
{
    public static function log($params, $response, $method)
    {
        $log_str = date('Y-m-d H:i:s') . ' | REQUEST: METHOD: ' . $method . ' ' . serialize($params) . PHP_EOL
            . date('Y-m-d H:i:s') . ' | RESPONSE: METHOD: ' . $method . ' ' . serialize($response) . PHP_EOL
            . '---------------------------------------------------------------------------------------------------------' . PHP_EOL;

        file_put_contents(ROOT_PATH . '/protected/runtime/apilog.txt', $log_str, FILE_APPEND);
    }
}