<?php

class GoogleTagManagerWidget extends CWidget
{

    const CLIENT_ID_KEY = 'clid';


    private $utm_cookies = ['utm_source', 'utm_campaign', 'utm_term', 'utm_medium', 'utm_content'];


    public function run()
    {
        $conference_id = Yii::app()->request->getParam('conf_id');
        $room_id = Yii::app()->request->getParam('id');
        $category = '12 - Вебинар1 - просмотр';

        $conference = ApiClient::getConferenceInfo($conference_id);

        $room = ApiClient::getRoomInfo($room_id);

        if (in_array($room['room']['state_id'], [2, 21, 22, 3, 4])) {
            $category = '13 - Вебинар2 - просмотр';
        } elseif (in_array($room['room']['state_id'], [5, 6])) {
            $category = '14 - Вебинар3 - просмотр';
        }

        $cookie_collection = Yii::app()->request->getCookies();
        $utm_cookie_string = '';

        foreach ($this->utm_cookies as $utm_cookie) {

            $utm_cookie_string .= $utm_cookie . ':' . $cookie_collection[$utm_cookie] . '|';
        }

        $this->render('googleTagManagerWidget', [
            'category' => $category,
            'direction_id' => $conference['direction_id'],
            'client_id' => isset($_COOKIE[self::CLIENT_ID_KEY]) ? $_COOKIE[self::CLIENT_ID_KEY] : 'undefined',
            'room_id' => $room_id,
        ]);
    }
}