<?php

/**
 * Виджет выводит верхнюю панель
 * Class TopPanelWidget
 */
class TopPanelWidget extends CWidget {

    public function run()
    {
        $email_current_user = Yii::app()->user->email;
        $this->render('topPanelWidget', array('email_current_user' => $email_current_user));
    }
} 