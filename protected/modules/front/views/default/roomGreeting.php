<div class="greeting-speaker">

    <?php if (! in_array($conference['type_id'], [ ConferenceHelper::CONFERENCE_TYPE_CLOSE, ConferenceHelper::CONFERENCE_TYPE_OPEN ])): ?>

        <div class="row conference-name">
            <p><?= $room['number_in_folder'] ?>-й день конференции <?= $conference['name']; ?></p>
        </div>

        <div class="row conference-date">
            <p><?= $room['date'] ?> с <?= (isset($performances[0])) ? substr($performances[0]['date'], 11, 5) : '' ?> (мск)</p>
        </div>

    <?php endif; ?>

    <?php foreach ($performances as $key => $performance): ?>
        <div class="row row-speaker">
            <div class="col-xs-2 conference-speaker">
                <div class="row speaker-photo">
                    <div class="speaker-front-photo" style="background-image: url(<?= $performance['speaker_img'] ?>)"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 speaker-name">
                        <p><?= $performance['speaker_name']; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 conference-info">
                <div class="row">
                    <div class="col-xs-12 conference-info-date">
                        <div class="conference-time">
                            <p><?= $room['date'] ?> с <?= substr($performance['date'], 11, 5) ?></p>
                        </div>
                        <div class="conference-region">
                            <p>Время московское</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 speaker-theme">
                        <p><?= $performance['theme'] ?></p>
                    </div>
                </div>

                <div class="row conference-features">

                    <?php if(!empty($performance['description'])): ?>
                        <div class="col-xs-12 features-label">
                            <p>Из мастер - класса Вы узнаете:</p>
                        </div>
                        <div class="col-xs-10 features">
                            <?= BulletHtml::get($performance['description']) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (!empty($performance['video'])): ?>

                        <div class="col-xs-10 features">

                            <?= preg_replace(["/width=\"\d+\"/", "/height=\"\d+\"/"], ['width="480"', 'height="360"'], $performance['video']) ?>

                        </div>

                    <?php endif; ?>

                    <?php if(!empty($performance['materials']) || !empty($performance['files'])): ?>
                        <div class="col-xs-12 conference-materials">
                            <div class="col-xs-12 materials-label">
                                <p>Материалы и инструменты для мастер - класса:
                                    <?php
                                    if (!empty($performance['files'])) {
                                        $files = explode(',', $performance['files']);

                                        if (count($files) === 1) {
                                            echo '<a target="_blank" href="' . $files[0] . '">Скачать материалы</a>';
                                        } else {
                                            $index = 1;

                                            foreach ($files as &$file) {
                                                $file = '<a target="_blank" href="' . $file . '">Файл' . $index++ . '</a>';
                                            }

                                            echo implode(', ', $files);
                                        }
                                    }
                                    ?>
                                </p>
                            </div>
                            <?php if (!empty($performance['materials'])) : ?>
                                <div class="col-xs-10 features">
                                    <?= BulletHtml::get($performance['materials']) ?>
                                </div>
                            <?php endif; ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </div>
        <?php if(!empty($performance['images'])): ?>
            <div class="row spaker-images">
                <div class="col-xs-offset-2 col-xs-10">

                    <?php foreach (explode(',', $performance['images']) as $image): ?>
                        <a class="fancybox-image-container" href="<?= $image ?>">
                            <img class="speaker-image" src="<?= $image ?>">
                        </a>
                    <?php endforeach; ?>

                </div>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>
</div>