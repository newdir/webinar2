<?php if (empty($speakers)): ?>
    <p>В данной комнате пока нет ни одного спикера.</p>
<?php else: ?>

    <?php foreach($speakers as $speaker): ?>

        <form class="form-horizontal form-edit-speaker content-speaker">
            <div class="col-xs-1 wrap-img-circle">
                <img class="img-circle" src="<?= $speaker['speaker_img']; ?>" width="55" height="55">
            </div>
            <div class="col-xs-10 col-xs-offset-1 speaker-info">
                <div class="form-group">
                    <div class="col-xs-6">
                        <p class="info-no-edit"><?= $speaker['speaker_name']; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-2">
                        <p class="clock info-no-edit"><?= Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", strtotime($speaker['date'])); ?><img src="/img/icon/time.png"></p>
                    </div>
                    <div class="col-xs-8 info-edit" style="display: none;">
                        <p class="content-speakers-help">Московское время (UTC+4). Для других часовых поясов рассчитается автоматически.</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-8">
                        <p class="title info-no-edit"><b><?= $speaker['theme']; ?></b></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-8">
                        <!--<p class="speaker-info-structure info-no-edit">-->
                            <?= BulletHtml::get($speaker['description']); ?>
                        <!--</p>-->
                    </div>
                    <div class="col-xs-3 info-edit" style="display: none;">
                        <p class="content-speakers-help">Один пункт в одной строке.<br>Не ставьте знаки препинания  в конце.</p>
                    </div>
                </div>
            </div>
        </form>

    <?php endforeach; ?>

<?php endif; ?>