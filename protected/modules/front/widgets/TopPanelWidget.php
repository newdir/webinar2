<?php

class TopPanelWidget extends CWidget
{
    public function run()
    {
        Yii::app()->clientScript->registerScriptFile('/js/header.js', CClientScript::POS_END);

        $conference_id = Yii::app()->request->getParam('conf_id');
        $room_id = Yii::app()->request->getParam('id');

        $room = ApiClient::getRoomInfo($room_id);

        $count_online = 0;
        $is_watch_page = false;

        $pseudo_online = Yii::app()->redis->getClient()->get('room:' . $room['room']['id'] . ':lie-online');

        if ($pseudo_online !== false) {
            $count_online += $pseudo_online;
        }

        $room_ips = new ARedisSet('room:' . $room['room']['id'] . ':ips');

        $count_online += $room_ips->count();

        if (Yii::app()->request->getParam('hw', false)) {
            $is_watch_page = true;
        }

        $conference_info = ApiClient::getConferenceInfo($conference_id);

        $this->render('topPanelWidget', [
            'count_online' => $count_online,
            'is_watch_page' => $is_watch_page,
            'room' => $room['room'],
            'conference' => $conference_info,
        ]);
    }
} 