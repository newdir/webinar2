<?php

class BackModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'back.models.*',
			'back.components.*',
		));
	}
}
