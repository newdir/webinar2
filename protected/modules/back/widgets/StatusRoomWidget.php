<?php

class StatusRoomWidget extends CWidget {

    public $buttons = array(
        RoomHandler::ROOM_STATE_CLOSE => 'Закрыт',
        RoomHandler::ROOM_STATE_GREETING => 'Приветствие',
        RoomHandler::ROOM_STATE_STREAM_1 => 'Тр #1',
        RoomHandler::ROOM_STATE_STREAM_2 => 'Тр #2',
        RoomHandler::ROOM_STATE_STREAM_3 => 'Тр #3',
        RoomHandler::ROOM_STATE_VIEW_RECORDS => 'Просмотр записей',
        RoomHandler::ROOM_STATE_TECH_PAUSE => 'Перерыв 10 минут',
        RoomHandler::ROOM_STATE_TROUBLE => 'Техпроблемы',
        RoomHandler::ROOM_STATE_BUY_RECORDS => 'Продажа записей',
        RoomHandler::ROOM_STATE_POSTPONED => 'Перенесено',
        RoomHandler::ROOM_STATE_MK_IS_OVER => 'МК окончен'
    );

    public $room_model;

    public function run()
    {
        $this->render('statusRoomWidget', array('buttons' => $this->buttons, 'room' => $this->room_model));
    }
}