<?php

/**
 * Класс принимает запросы на модификацию информации о папке
 * Class FolderController
 */
class FolderController extends Controller {

    public function filters()
    {
        return [
            'accessControl',
            ['application.modules.back.controllers.filters.ConferenceAccessControlFilter'],
            ['application.modules.back.controllers.filters.RequestFilter'],
        ];
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?')
            ),
        );
    }

    public function actionView($conf_id)
    {
        $rooms = ApiClient::getRoomList($conf_id);

        if (empty($rooms)) {

            $room_id = ApiClient::createRoom($conf_id, 'Пустая комната', null, 0, false);

        } else {

            usort($rooms, function ($a, $b) {
                if ($a['id'] === $b['id']) {
                    return 0;
                }

                return $a['id'] > $b['id'] ? -1 : 1;
            });

            $room_id = reset($rooms)['id'];
        }

        $this->redirect('/admin/' . $conf_id .'/room/'.$room_id);
    }
}