<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 07.09.16
 * Time: 16:36
 */

class VisitRegistrator
{
    public static function registerComing($conference_id, $csp_id)
    {
        $client_id = self::getClientId($conference_id);

        if (is_null($client_id) || is_null($csp_id)) {
            return;
        }

        $tmp_visit = Yii::app()->db->createCommand()
            ->select('id')
            ->from('tmp_visit_duration')
            ->where('client_id = :client_id and conference_speaker_performance_id = :csp_id', [
                ':client_id' => $client_id,
                ':csp_id' => $csp_id,
            ])
            ->queryScalar();

        try {

            if ($tmp_visit === false) {

                Yii::app()->db->createCommand()
                    ->insert('tmp_visit_duration', [
                        'client_id' => $client_id,
                        'conference_speaker_performance_id' => $csp_id,
                        'come_time' => time(),
                    ]);
            } else {

                Yii::app()->db->createCommand()
                    ->update('tmp_visit_duration',
                        ['come_time' => time()],
                        'client_id = :client_id and conference_speaker_performance_id = :csp_id',
                        [':client_id' => $client_id, ':csp_id' => $csp_id]
                    );
            }

        } catch (CException $e) {}
    }

    private static function getClientId($conference_id)
    {
        $cookie = Yii::app()->request->cookies[md5('registered_' . $conference_id)];

        return is_null($cookie) ? null : $cookie->value;
    }
}