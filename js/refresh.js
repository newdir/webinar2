function Refresh()
{
    this.delay = 1 * 60 * 1000;
    this.states = {};
    this.state_url_old_browser = null;
    this.state_url_sockets = 'state/';
    this.content_url = 'content/';
    this.old_browser = $('html').hasClass('websockets');
    this.resources = {};
    this.onlineCallback = function(data){};
}

Refresh.prototype.run = function()
{
    var that = this;

    if (that.resources == {}) {
        setTimeout(function() { that.run(); }, that.delay);
    }

    var state_url = (this.old_browser)
        ? this.state_url_sockets
        : this.state_url_old_browser;

    $.ajax({
        url : state_url,
        type: "GET",
        dataType : 'json',
        cache : false,
        success: function(data)
        {
            that.checkState(data.state);
            that.onlineCallback(data.onine);
            setTimeout(function() { that.run(); }, that.delay);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log('Something goes wrong. Refresh the page, please.');
        }
    });
}

/**
 * Функция проверяет схождение/расхождение состояния на клиенте и сервере
 */
Refresh.prototype.checkState = function(state)
{
    var that = this,
        resources = []
        ;
    $.each(this.resources, function(i, el) {
        var curr_state = (that.states[i] == undefined) ? 0 : that.states[i],
            new_state = state[i];
        ;
        if (new_state == curr_state)
            return;

        resources.push(i);
    });

    $.ajax({
        url : that.content_url,
        type: "GET",
        dataType : 'json',
        cache : false,
        data : {resources : resources},
        success: function(data)
        {
            if (data.success != true) {
                console.log('Что-то пошло не так. Пожалуйста, перезагрузите страницу');
            }
            $.each(data.resources, function(i, el) {
                that.resources[i].state = state[i];
                if (that.resources[i].callback != undefined)
                    that.resources[i].callback();
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log('Что-то пошло не так. Пожалуйста, перезагрузите страницу');
        }
    });
}