<div class="room-content" style="padding: 15px">
    <h1>Мастер-класс окончен</h1>
    <br>
    <p style="font-size: 18px">Запись мастер-класса появится в течение суток у вас в личном кабинете. Смотрите информацию по заказу в разделе "История заказов" - <a href="https://newdirections.ru/user/orderhistory">https://newdirections.ru/user/orderhistory</a></p>
</div>
