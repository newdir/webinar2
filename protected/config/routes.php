<?php

return array(
    array('/back/default/index', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/', 'verb' => 'GET'),

    array('/back/folder/view', 'pattern' => 'admin/<conf_id:\w+>/', 'verb'=>'GET'),

    array('/back/room/create', 'pattern' => 'admin/<conf_id:\w+>/room/', 'verb'=>'GET'),
    array('/back/room/create', 'pattern' => 'admin/<conf_id:\w+>/room/', 'verb'=>'POST'),
    array('/back/room/loadSpeakers', 'pattern' => 'admin/<conf_id:\w+>/room/loadspeakers/', 'verb'=>'POST'),
    array('/back/room/loadSpeakers', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/loadspeakers/', 'verb'=>'POST'),
    array('/back/room/update', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/', 'verb'=>'POST'),
    array('/back/default/error', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/error/', 'verb'=>'POST'),
    array('/back/default/deleteRoom', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/deleteroom/', 'verb'=>'POST'),
    array('/back/default/setPseudoOnline', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/setpseudoonline/', 'verb'=>'POST'),
    array('/back/room/savePerfomance', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/perfomance/', 'verb'=>'POST'),
    array('/back/room/clearCache', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/clearcache/', 'verb'=>'POST'),

    array('/back/banner/save', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/banner/', 'verb' => 'POST'),
    array('/back/banner/save', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/banner/<id_banner:\d+>/', 'verb' => 'POST'),
    array('/back/banner/destroy', 'pattern' => 'admin/<conf_id:\w+>/room/<id:\d+>/banner/<id_banner:\d+>/', 'verb' => 'DELETE'),


    array('/front/default/unload', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/unload/', 'verb'=>'POST'),
    array('/front/default/registerComing', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/coming/', 'verb'=>'POST'),
    array('/front/default/index', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/', 'verb'=>'GET'),
    array('/front/default/shedule', 'pattern' => 'conference/<id:\d+>/schedule/', 'verb'=>'GET'),
    array('/front/default/content', 'pattern' => '<conf_id:\w+>/<id:\d+>/content/', 'verb'=>'GET'),
    array('/front/default/state', 'pattern' => '<conf_id:\w+>/<id:\d+>/content/', 'verb'=>'GET'),
    array('/front/default/watch', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/watch/', 'verb'=>'GET'),
    array('/front/default/speaker', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/speaker/', 'verb'=>'GET'),
    array('/front/default/record', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/record/<record_id:\d+>', 'verb'=>'GET'),
    array('/front/default/record', 'pattern' => '<conf_id:\w+>/room/<id:\d+>/record/<record_id:\d+>', 'verb'=>'GET'),
    array('/front/default/countOnline', 'pattern' => '/count_online/', 'verb'=>'GET'),

    array('/user/login', 'pattern' => 'login/', 'verb'=>'POST'),
    array('/user/login', 'pattern' => 'login/', 'verb'=>'GET'),
    array('/user/logout', 'pattern' => 'logout/', 'verb'=>'GET'),
);