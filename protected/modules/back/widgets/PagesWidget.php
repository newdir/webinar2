<?php
/**
 * Created by PhpStorm.
 * User: artyomnorin
 * Date: 20.08.15
 * Time: 13:25
 *
 * Виджет для формирования блока ссылок на страницу для менеджера, посетителей, спикера, продажи записей
 */

class PagesWidget extends CWidget{

    public function run()
    {
        $conf_id = Yii::app()->request->getParam('conf_id');
        $room_id = Yii::app()->request->getParam('id');

        $room = ApiClient::getAdminRoom($conf_id, $room_id);

        $records_hash = [];

        if (!empty($room['performances'])) {
            foreach ($room['performances'] as $performance) {
                $records_hash[] = md5($conf_id . $room_id. $performance['key_hangouts']);
            }
        }

        $base_url = '/'.$conf_id.'/room/'.$room_id.'/';
        $speaker_link = 'https://newdirections.ru/webinars/room/' . $room_id . '?is_speaker=1';
        $client_link = 'https://newdirections.ru/webinars/room/' . $room_id;

        if ($room['is_private']) {
            $hash = md5($conf_id . $room_id . '_rmprvt');
            $speaker_link .= ('&hash=' . $hash);
        }

        $this->render('pagesWidget', array(
                'base_url' => $base_url,
                'records_hash' => $records_hash,
                'speaker_link' => $speaker_link,
                'client_link' => $client_link,
                'room_id' => $room_id
            )
        );
    }
}