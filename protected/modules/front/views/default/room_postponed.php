<?php
/**
 * @var $date_postpone
 */
?>
<div class="room-content" style="padding: 15px">
    <h1>Выступление перенесено</h1>
    <br>
    <p style="font-size: 18px">Уважаемые гости! По техническим причинам выступление спикера перенесено на <?= date('d.m.Y H:i', strtotime($date_postpone)) ?>. Приносим извинения за неудобства!</p>
</div>