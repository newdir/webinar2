<?php

class UserHelper
{

    public static function get()
    {
        return [
            'id' => Yii::app()->user->getState('id'),
            'access_level' => Yii::app()->user->getState('access_level'),
            'nick_name' => Yii::app()->user->getState('nick_name'),
            'email' => Yii::app()->user->getState('email'),
        ];
    }

    public static function getHash($user = null)
    {
        $user = ($user) ? $user : UserHelper::get();
        return md5('smd5' . $user['id'] . 'resu');
    }
}