/**
 * Created by artyomnorin on 27.07.15.
 */

var App = App || {};
var EventManager = _.extend({}, Backbone.Events);

App = {
    Models: {},
    Collections: {},
    Views: {}
};